// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "ShooterTypes.h"
#include "ShooterCharacter.generated.h"

DECLARE_MULTICAST_DELEGATE_TwoParams(FOnShooterCharacterEquipWeapon, AShooterCharacter*, AShooterWeapon* /* new */);
DECLARE_MULTICAST_DELEGATE_TwoParams(FOnShooterCharacterUnEquipWeapon, AShooterCharacter*, AShooterWeapon* /* old */);

UCLASS(Abstract)
class AShooterCharacter : public ACharacter
{
	GENERATED_UCLASS_BODY()

	virtual void BeginDestroy() override;

	/** spawn inventory, setup initial variables */
	virtual void PostInitializeComponents() override;

	/** Update the character. (Running, health etc). */
	virtual void Tick(float DeltaSeconds) override;

	/** cleanup inventory */
	virtual void Destroyed() override;

	/** update mesh for first person view */
	virtual void PawnClientRestart() override;

	/** [server] perform PlayerState related setup */
	virtual void PossessedBy(class AController* C) override;

	/** [client] perform PlayerState related setup */
	virtual void OnRep_PlayerState() override;

	/** [server] called to determine if we should pause replication this actor to a specific player */
	virtual bool IsReplicationPausedForConnection(const FNetViewer& ConnectionOwnerNetViewer) override;

	/** [client] called when replication is paused for this actor */
	virtual void OnReplicationPausedChanged(bool bIsReplicationPaused) override;

	/**
	* Add camera pitch to first person mesh.
	*
	*	@param	CameraLocation	Location of the Camera.
	*	@param	CameraRotation	Rotation of the Camera.
	*/
	void OnCameraUpdate(const FVector& CameraLocation, const FRotator& CameraRotation);

	/** get aim offsets */
	UFUNCTION(BlueprintCallable, Category = "Game|Weapon")
	FRotator GetAimOffsets() const;

	/**
	* Check if pawn is enemy if given controller.
	*
	* @param	TestPC	Controller to check against.
	*/
	bool IsEnemyFor(AController* TestPC) const;

	//////////////////////////////////////////////////////////////////////////
	// Inventory

	/**
	* [server] add weapon to inventory
	*
	* @param Weapon	Weapon to add.
	*/
	void AddWeapon(class AShooterWeapon* Weapon);

	/**
	* [server] remove weapon from inventory
	*
	* @param Weapon	Weapon to remove.
	*/
	void RemoveWeapon(class AShooterWeapon* Weapon);

	/**
	* Find in inventory
	*
	* @param WeaponClass	Class of weapon to find.
	*/
	class AShooterWeapon* FindWeapon(TSubclassOf<class AShooterWeapon> WeaponClass);

	/**
	* [server + local] equips weapon from inventory
	*
	* @param Weapon	Weapon to equip
	*/
	void EquipWeapon(class AShooterWeapon* Weapon);

	//////////////////////////////////////////////////////////////////////////
	// Weapon usage

	/** [local] starts weapon fire */
	void StartWeaponFire();

	/** [local] stops weapon fire */
	void StopWeaponFire();

	/** check if pawn can fire weapon */
	bool CanFire() const;

	/** check if pawn can reload weapon */
	bool CanReload() const;

	/** [server + local] change targeting state */
	void SetTargeting(bool bNewTargeting);

	//////////////////////////////////////////////////////////////////////////
	// Movement

	/** [server + local] change running state */
	void SetRunning(bool bNewRunning, bool bToggle);

	//////////////////////////////////////////////////////////////////////////
	// Animations

	/** play anim montage */
	virtual float PlayAnimMontage(class UAnimMontage* AnimMontage, float InPlayRate = 1.f, FName StartSectionName = NAME_None) override;

	/** stop playing montage */
	virtual void StopAnimMontage(class UAnimMontage* AnimMontage) override;

	/** stop playing all montages */
	void StopAllAnimMontages();

	//////////////////////////////////////////////////////////////////////////
	// Input handlers

	/** setup pawn specific input handlers */
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	/**
	* Move forward/back
	*
	* @param Val Movment input to apply
	*/
	void MoveForward(float Val);

	/**
	* Strafe right/left
	*
	* @param Val Movment input to apply
	*/
	void MoveRight(float Val);

	/**
	* Move Up/Down in allowed movement modes.
	*
	* @param Val Movment input to apply
	*/
	void MoveUp(float Val);

	/* Frame rate independent turn */
	void TurnAtRate(float Val);

	/* Frame rate independent lookup */
	void LookUpAtRate(float Val);

	/* Turn */
	void Turn(float Val);

	/* Look up */
	void LookUp(float Val);

	/** player pressed start fire action */
	void OnStartFire();

	/** player released start fire action */
	void OnStopFire();

	/** player pressed targeting action */
	void OnStartTargeting();

	/** player released targeting action */
	void OnStopTargeting();

	/** player pressed next weapon action */
	void OnNextWeapon();

	/** player pressed prev weapon action */
	void OnPrevWeapon();

	/** player pressed reload action */
	void OnReload();

	/** player pressed jump action */
	void OnStartJump();

	/** player released jump action */
	void OnStopJump();

	/** player pressed run action */
	void OnStartRunning();

	/** player pressed toggled run action */
	void OnStartRunningToggle();

	/** player released run action */
	void OnStopRunning();

	//////////////////////////////////////////////////////////////////////////
	// Reading data

	/** get mesh component */
	USkeletalMeshComponent* GetPawnMesh() const;

	/** get currently equipped weapon */
	UFUNCTION(BlueprintCallable, Category = "Game|Weapon")
	class AShooterWeapon* GetWeapon() const;

	/** Global notification when a character equips a weapon. Needed for replication graph. */
	SHOOTERGAME_API static FOnShooterCharacterEquipWeapon NotifyEquipWeapon;

	/** Global notification when a character un-equips a weapon. Needed for replication graph. */
	SHOOTERGAME_API static FOnShooterCharacterUnEquipWeapon NotifyUnEquipWeapon;

	/** get weapon attach point */
	FName GetWeaponAttachPoint() const;

	/** get total number of inventory items */
	int32 GetInventoryCount() const;

	/**
	* get weapon from inventory at index. Index validity is not checked.
	*
	* @param Index Inventory index
	*/
	class AShooterWeapon* GetInventoryWeapon(int32 index) const;

	/** get weapon taget modifier speed	*/
	UFUNCTION(BlueprintCallable, Category = "Game|Weapon")
	float GetTargetingSpeedModifier() const;

	/** get targeting state */
	UFUNCTION(BlueprintCallable, Category = "Game|Weapon")
	bool IsTargeting() const;

	/** get firing state */
	UFUNCTION(BlueprintCallable, Category = "Game|Weapon")
	bool IsFiring() const;

	/** get the modifier value for running speed */
	UFUNCTION(BlueprintCallable, Category = Pawn)
	float GetRunningSpeedModifier() const;

	/** get running state */
	UFUNCTION(BlueprintCallable, Category = Pawn)
	bool IsRunning() const;

	/** get camera view type */
	UFUNCTION(BlueprintCallable, Category = Mesh)
	virtual bool IsFirstPerson() const;

	/** get max health */
	int32 GetMaxHealth() const;

	/** check if pawn is still alive */
	bool IsAlive() const;

	/** returns percentage of health when low health effects should start */
	float GetLowHealthPercentage() const;

	/*
	* Get either first or third person mesh.
	*
	* @param	WantFirstPerson		If true returns the first peron mesh, else returns the third
	*/
	USkeletalMeshComponent* GetSpecifcPawnMesh(bool WantFirstPerson) const;

	/** Update the team color of all player meshes. */
	void UpdateTeamColorsAllMIDs();
private:

	/** pawn mesh: 1st person view */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	USkeletalMeshComponent* Mesh1P;
protected:

	/** socket or bone name for attaching weapon mesh */
	UPROPERTY(EditDefaultsOnly, Category = Inventory)
	FName WeaponAttachPoint;

	/** default inventory list */
	UPROPERTY(EditDefaultsOnly, Category = Inventory)
	TArray<TSubclassOf<class AShooterWeapon> > DefaultInventoryClasses;

	/** weapons in inventory */
	UPROPERTY(Transient, Replicated)
	TArray<class AShooterWeapon*> Inventory;

	/** currently equipped weapon */
	UPROPERTY(Transient, ReplicatedUsing = OnRep_CurrentWeapon)
	class AShooterWeapon* CurrentWeapon;

	/** Replicate where this pawn was last hit and damaged */
	UPROPERTY(Transient, ReplicatedUsing = OnRep_LastTakeHitInfo)
	struct FTakeHitInfo LastTakeHitInfo;

	/** Time at which point the last take hit info for the actor times out and won't be replicated; Used to stop join-in-progress effects all over the screen */
	float LastTakeHitTimeTimeout;

	/** modifier for max movement speed */
	UPROPERTY(EditDefaultsOnly, Category = Inventory)
	float TargetingSpeedModifier;

	/** current targeting state */
	UPROPERTY(Transient, Replicated)
	uint8 bIsTargeting : 1;

	/** modifier for max movement speed */
	UPROPERTY(EditDefaultsOnly, Category = Pawn)
	float RunningSpeedModifier;

	/** current running state */
	UPROPERTY(Transient, Replicated)
	uint8 bWantsToRun : 1;

	/** from gamepad running is toggled */
	uint8 bWantsToRunToggled : 1;

	/** current firing state */
	uint8 bWantsToFire : 1;

	/** when low health effects should start */
	float LowHealthPercentage;

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	float BaseTurnRate;

	/** Base lookup rate, in deg/sec. Other scaling may affect final lookup rate. */
	float BaseLookUpRate;

	/** material instances for setting team color in mesh (3rd person view) */
	UPROPERTY(Transient)
	TArray<UMaterialInstanceDynamic*> MeshMIDs;

	/** animation played on death */
	UPROPERTY(EditDefaultsOnly, Category = Animation)
	UAnimMontage* DeathAnim;

	/** sound played on death, local player only */
	UPROPERTY(EditDefaultsOnly, Category = Pawn)
	USoundCue* DeathSound;

	/** effect played on respawn */
	UPROPERTY(EditDefaultsOnly, Category = Pawn)
	UParticleSystem* RespawnFX;

	/** sound played on respawn */
	UPROPERTY(EditDefaultsOnly, Category = Pawn)
	USoundCue* RespawnSound;

	/** sound played when health is low */
	UPROPERTY(EditDefaultsOnly, Category = Pawn)
	USoundCue* LowHealthSound;

	/** sound played when running */
	UPROPERTY(EditDefaultsOnly, Category = Pawn)
	USoundCue* RunLoopSound;

	/** sound played when stop running */
	UPROPERTY(EditDefaultsOnly, Category = Pawn)
	USoundCue* RunStopSound;

	/** sound played when targeting state changes */
	UPROPERTY(EditDefaultsOnly, Category = Pawn)
	USoundCue* TargetingSound;

	/** used to manipulate with run loop sound */
	UPROPERTY()
	UAudioComponent* RunLoopAC;

	/** hook to looped low health sound used to stop/adjust volume */
	UPROPERTY()
	UAudioComponent* LowHealthWarningPlayer;

	/** handles sounds for running */
	void UpdateRunSounds();

	/** handle mesh visibility and updates */
	void UpdatePawnMeshes();

	/** handle mesh colors on specified material instance */
	void UpdateTeamColors(UMaterialInstanceDynamic* UseMID);

	/** Responsible for cleaning up bodies on clients. */
	virtual void TornOff();

private:

	/** Whether or not the character is moving (based on movement input). */
	bool IsMoving();

	//////////////////////////////////////////////////////////////////////////
	// Damage & death

public:

	/** Identifies if pawn is in its dying state */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Health)
	uint32 bIsDying : 1;

	// Current health of the Pawn
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, Category = Health)
	float Health;

	/** Take damage, handle death */
	virtual float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, class AActor* DamageCauser) override;

	/** Pawn suicide */
	virtual void Suicide();

	/** Kill this pawn */
	virtual void KilledBy(class APawn* EventInstigator);

	/** Returns True if the pawn can die in the current state */
	virtual bool CanDie(float KillingDamage, FDamageEvent const& DamageEvent, AController* Killer, AActor* DamageCauser) const;

	/**
	* Kills pawn.  Server/authority only.
	* @param KillingDamage - Damage amount of the killing blow
	* @param DamageEvent - Damage event of the killing blow
	* @param Killer - Who killed this pawn
	* @param DamageCauser - the Actor that directly caused the damage (i.e. the Projectile that exploded, the Weapon that fired, etc)
	* @returns true if allowed
	*/
	virtual bool Die(float KillingDamage, struct FDamageEvent const& DamageEvent, class AController* Killer, class AActor* DamageCauser);

	// Die when we fall out of the world.
	virtual void FellOutOfWorld(const class UDamageType& dmgType) override;

	/** Called on the actor right before replication occurs */
	virtual void PreReplication(IRepChangedPropertyTracker & ChangedPropertyTracker) override;
protected:
	/** notification when killed, for both the server and client. */
	virtual void OnDeath(float KillingDamage, struct FDamageEvent const& DamageEvent, class APawn* InstigatingPawn, class AActor* DamageCauser);

	/** play effects on hit */
	virtual void PlayHit(float DamageTaken, struct FDamageEvent const& DamageEvent, class APawn* PawnInstigator, class AActor* DamageCauser);

	/** switch to ragdoll */
	void SetRagdollPhysics();

	/** sets up the replication for taking a hit */
	void ReplicateHit(float Damage, struct FDamageEvent const& DamageEvent, class APawn* InstigatingPawn, class AActor* DamageCauser, bool bKilled);

	/** play hit or death on client */
	UFUNCTION()
	void OnRep_LastTakeHitInfo();

	//////////////////////////////////////////////////////////////////////////
	// Inventory

	/** updates current weapon */
	void SetCurrentWeapon(class AShooterWeapon* NewWeapon, class AShooterWeapon* LastWeapon = NULL);

	/** current weapon rep handler */
	UFUNCTION()
	void OnRep_CurrentWeapon(class AShooterWeapon* LastWeapon);

	/** [server] spawns default inventory */
	void SpawnDefaultInventory();

	/** [server] remove all weapons from inventory and destroy them */
	void DestroyInventory();

	/** equip weapon */
	UFUNCTION(reliable, server, WithValidation)
	void ServerEquipWeapon(class AShooterWeapon* NewWeapon);

	/** update targeting state */
	UFUNCTION(reliable, server, WithValidation)
	void ServerSetTargeting(bool bNewTargeting);

	/** update targeting state */
	UFUNCTION(reliable, server, WithValidation)
	void ServerSetRunning(bool bNewRunning, bool bToggle);

	/** Builds list of points to check for pausing replication for a connection*/
	void BuildPauseReplicationCheckPoints(TArray<FVector>& RelevancyCheckPoints);

protected:
	/** Returns Mesh1P subobject **/
	FORCEINLINE USkeletalMeshComponent* GetMesh1P() const { return Mesh1P; }

	//////////////////////////////////////////////////////////////////////////
	// ###################   Added methods  #########################

	//////////////////////////////////////////////////////////////////////////
	// Teleport

public:

	// Character Movement Component
	class UShooterCharacterMovement* ShooterCharacterMovement;

	// Check if teleport ability is not in cooldown
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Teleport")
	bool bCanTeleport;

	// Cooldown time before using teleport again
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Teleport")
	float TeleportCooldown = 3.f;

protected:

	// Player pressed teleport action
	void OnTeleport();

	// Cooldown of the ability finished
	void TeleportCooldownFinished();

	//////////////////////////////////////////////////////////////////////////
	// Rewind

public:

	// Check if rewind ability is not in cooldown
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Rewind")
	bool bCanRewind;

	// Cooldown time before using rewind again
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Rewind")
	float RewindCooldown = 6.f;

	// Amount of seconds the player can rewind in time
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Rewind")
	float RewindTimeSpan = 5.f;

	// Time needed to rewind
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Rewind")
	float RewindTime = 1.f;

protected:

	// Memorized past locations to use for rewind
	UPROPERTY(Replicated)
	TArray<FVector> PastLocations;

	// Time between each past location
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Rewind")
	float StoreLocationTimeOffset = 0.5f;

	// True if the player is currently using the ability
	bool bInRewind;

	// Max locations to memorize
	int32 MaxLocations;

	// Index to access past locations
	int32 LocIndex;

	// Handle for StoreCurrentLocation() timer
	FTimerHandle StoreLocationsHandle;

	// Handle for RewindStep() timer
	FTimerHandle RewindHandle;

	// [Server] Memorize the current location every 0.5 seconds
	void StoreCurrentLocation();

	// Player pressed rewind action
	void OnRewind();

	// [Server] Start the rewind on server
	UFUNCTION(Reliable, Server)
	void ServerRewind();

	// Start the rewind process
	void Rewind();

	// Move the player to a past location
	void RewindStep();

	// Cooldown of the ability has finished
	void RewindCooldownFinished();

	//////////////////////////////////////////////////////////////////////////
	// Jetpack

	// Check if the player can currently using the jetpack
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Jetpack")
	bool bCanJetpack;

	// Wait before being able to use the jetpack again (to avoid flooding of server rpc calls)
	void JetpackWait();

	//////////////////////////////////////////////////////////////////////////
	// Freeze Gun

	// Particle system component for freezing graphic effects
	UPROPERTY(VisibleAnywhere)
	UParticleSystemComponent *FreezePSComponent;

	// Broken ice graphic effect
	UPROPERTY(EditDefaultsOnly, Category = "Freezing")
	UParticleSystem *FreezeBrokenParticleSystem;

	// Duration of freezing effect
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Freezing")
	float FreezeTime = 5.f;

	// The player freezing status, when this is true movement inputs are not allowed
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, ReplicatedUsing=OnRep_IsFreezing, Category = "Status")
	bool bIsFreezing;

	// Freezing camera shake effect when trying to move
	UPROPERTY(EditDefaultsOnly, Category = "Freezing")
	TSubclassOf<UCameraShake> FreezeCameraShake;

	// [Server] Set the player status to frozen for a certain amount of time 
	void ServerFreeze();

	// [Server] Set the player status to unfrozen
	void ServerUnfreeze();

	// Handle graphic effects everytime bIsFreezing change
	UFUNCTION()
	void OnRep_IsFreezing();

	// Generate freezing graphic effects
	void FreezeEffects();

	// Remove freezing graphic effects
	void UnfreezeEffects();
	
	//////////////////////////////////////////////////////////////////////////
	// Shrink Gun
	
public:

	// Check if the player is currently shrinked
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, ReplicatedUsing = OnRep_Shrink, Category = "Status")
	bool bIsShrinked;

	// Time remaining before returning to normal size
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Shrinking")
	float ShrinkRemainingTime;

protected:

	// Scale to use for shrinking
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Shrinking")
	float ShrinkScale = 0.1f;

	// Offset to render the arms correctly when shrinked
	UPROPERTY(VisibleAnywhere, Category = "Shrinking")
	FVector ShrinkMeshOffset = FVector(25.f, 0.f, 0.f);

	// Time of the interpolation for the shrinking process
	UPROPERTY(VisibleAnywhere, Category = "Shrinking")
	float ShrinkingInterpolationTime = 0.5f;

	// View height when shrinked
	UPROPERTY(VisibleAnywhere, Category = "Shrinking")
	float ShrinkEyeHeight = 6.35f;

	// Time before returning to normal scale
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Shrinking")
	float ShrinkTime = 10.f;

	// Holder variable to mantain the default eye height
	float DefaultEyeHeight;

	// Check if the player is currently shrinking by interpolation
	bool bInShrinkingProcess;

	// Initial scale before interpolation process
	FVector InitialScale;

	// Scale after interpolation ended
	FVector TargetScale;

	// Initial eye height before interpolation
	float InitialEyeHeight;

	// Eye height after interpolation
	float TargetEyeHeight;

	// Handle for the shrinking interpolation timer
	FTimerHandle ShrinkingProcessHandle;

	// Handle for the shrinking timer
	FTimerHandle ShrinkTimerHandle;

	// [Server] Set the player status to shrinked for a certain amount of time
	void ServerShrink();

	// [Server] Set the player status to unshrinked
	void ServerUnshrink();

	// [Client] Change the shrinking effects when bIsShrinking change
	UFUNCTION()
	void OnRep_Shrink();

	// Start the shrinking interpolation
	void StartShrinkingProcess();

	// Start the unshrinking interpolation
	void StartUnshrinkingProcess();

	// Apply shrinking effects to the player
	void Shrink();

	// Apply deshrinking effects to the player
	void Unshrink();

	virtual void NotifyActorBeginOverlap(class AActor* Other) override;

	//////////////////////////////////////////////////////////////////////////
	// Weapon Pickup

	// Weapon pickup to spawn
	UPROPERTY(EditDefaultsOnly, Category = Pickup)
	TSubclassOf<class AShooterPickup_Weapon> WeaponPickup;

	// [Server] Spawn a weapon pickup
	void SpawnWeaponPickup();

	//////////////////////////////////////////////////////////////////////////
	// Wall Run

public:

	// Cooldown time before running again
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Wall Run")
	float WallRunCooldown = 1.f;

	// Duration of the run before start falling
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Wall Run")
	float WallRunDuration = 0.5f;

	// Distance in centimeters of the run before start falling
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Wall Run")
	float WallRunDistance = 500.f;

protected:

	// Check if wall run is not in cooldown
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	bool bCanWallRun;

	// Check if player is currently in wall run
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	bool bInWallRun;

	// Check if the character is currently touching a wall while in air
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	bool bOnWall;

	// Normal of the wall currently touched
	FVector WallNormal;

	// Check if the player already runned on the current wall
	bool bNewWall;

	// Check if the function NotifyHit was called this frame
	bool bNotifyHitCalled;

	// ID of the RootMotionSource used for the wall run
	uint8 WallRunForceID;

	virtual void NotifyHit(class UPrimitiveComponent* MyComp, AActor* Other, class UPrimitiveComponent* OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult& Hit) override;

	// [Client + Server] Apply a force to run on a wall
	void WallRun();

	// [Client + Server] Set bInWallRun to false
	void WallRunFinished();

	// [Client + Server] Stop the wall run
	void StopWallRun();

	// The cooldown of the ability has finished
	void WallRunCooldownFinished();


	//////////////////////////////////////////////////////////////////////////
	// Wall Jump

public:

	// Force applied for the wall jump
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Wall Jump")
	float WallJumpImpulseForce = 1000.f;

protected:
	
	// Check if the player can wall jump
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category="Wall Jump")
	bool bCanWallJump;

	// [Server] Call the wall jump method on the server
	UFUNCTION(Reliable, Server)
	void ServerWallJump();

	// [Client + Server] Apply a force to jump on a wall
	void WallJump();

	// Wait before wall jump can be executed again (to avoid flooding of server rpc calls)
	void WallJumpWait();

};


