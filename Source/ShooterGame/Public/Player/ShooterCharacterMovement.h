// Copyright Epic Games, Inc. All Rights Reserved.

/**
 * Movement component meant for use with Pawns.
 */

#pragma once
#include "ShooterCharacterMovement.generated.h"

UENUM(BlueprintType)
enum ECustomMovementMode {
	CUSTOM_Jetpack
};


UCLASS()
class UShooterCharacterMovement : public UCharacterMovementComponent
{
	GENERATED_UCLASS_BODY()

	virtual float GetMaxSpeed() const override;

	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction) override;

	virtual void OnMovementModeChanged(EMovementMode PreviousMovementMode, uint8 PreviousCustomMode) override;

	// Return true if the player is in jetpack mode
	UFUNCTION(BlueprintCallable, Category="Jetpack")
	bool InJetpack() const;

	// Return the remaining seconds of fuel remaining into the jetpack
	UFUNCTION(BlueprintCallable, Category="Jetpack")
	float GetCurrentFuel() const;

	// Return the maximum seconds of fuel storable into the jetpack
	UFUNCTION(BlueprintCallable, Category = "Jetpack")
	float GetMaxFuel() const;

	// Impulse force to apply for the jetpack
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Jetpack")
	float JetpackImpulseForce = 300000.f;

	// Maximum flying capacity of jetpack, in seconds
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Jetpack")
	float MaxFuel = 5.f;

	// Teleport distance in centimeters
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Teleport")
	float TeleportDistance = 1000.f;

	protected:

	virtual void PhysCustom(float DeltaTime, int32 Iterations) override;

	// True if the player is currently using the jetpack
	bool bInJetpack;

	// Current fuel available, determine the remaining seconds
	float CurrentFuel;
	
	// Handle the Jetpack fly mode
	void PhysJetpack(float DeltaTime, int32 Iterations);

	// Overridden to utilize custom FNetworkPredictionData to handle Client Prediction and Reconciliation
	virtual class FNetworkPredictionData_Client* GetPredictionData_Client() const override;

	// [Server] Extract state variables from a compressed flag on the server
	virtual void UpdateFromCompressedFlags(uint8 Flags) override;

	// Called for both client and server when performing movement.
	virtual void PerformMovement(float DeltaSeconds) override;

	public:

	// Check if the player pressed Jetpack key
	bool bPressedJetpack = false;

	// Check if the player pressed Teleport key
	bool bPressedTeleport = false;

	// [Client + Server] Handle various skill execution
	virtual void ExecuteSkills(float DeltaTime);

};

// Custom FSavedMove used to handle specific skill flags necessary to replicate moves
class FCustomSavedMove : public FSavedMove_Character
{
	public:
	typedef FSavedMove_Character Super;

	// Flags replicated to server using compressed flags
	bool bPressedTeleport;
	bool bPressedJetpack;

	// Reset flags to default values
	virtual void Clear() override;

	// Store state variable in a compressed flag sended to server
	virtual uint8 GetCompressedFlags() const override;

	// Check if the current move can be combined with an old one
	virtual bool CanCombineWith(const FSavedMovePtr& NewMove, ACharacter* Character, float MaxDelta) const override;

	// Initialize current move state using CharacterMovementComponent variables
	virtual void SetMoveFor(ACharacter* Character, float InDeltaTime, FVector const& NewAccel, class FNetworkPredictionData_Client_Character& ClientData) override;

	// Initialize CharacterMovementComponent variables using current move state, so that is possible to replay the move during reconciliation
	virtual void PrepMoveFor(class ACharacter* Character) override;
};

// Custom FNetworkPredictionData necessary to utilize custom FSavedMove
class FCustomNetworkPredictionData_Client : public FNetworkPredictionData_Client_Character
{
	public:
	typedef FNetworkPredictionData_Client_Character Super;

	// Allocates a new FCustomSavedMove
	virtual FSavedMovePtr AllocateNewMove() override;

	// Constructor
	FCustomNetworkPredictionData_Client(const class UCharacterMovementComponent& ClientMovement) : FNetworkPredictionData_Client_Character(ClientMovement) {};
};