// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ShooterPickup.h"
#include "ShooterPickup_Weapon.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class SHOOTERGAME_API AShooterPickup_Weapon : public AShooterPickup
{
	GENERATED_UCLASS_BODY()	

	virtual void PostInitializeComponents() override;

	virtual void GetLifetimeReplicatedProps(TArray< FLifetimeProperty >& OutLifetimeProps) const override;

	// Initialize pickup with weapon ammo and mesh
	UFUNCTION(BlueprintCallable, Category="Pickup")
	void SetWeapon(const class AShooterWeapon* Weapon);

	protected:

	// Lifetime of the pickup
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Pickup")
	float Lifetime = 5.f;

	// Type of the weapon
	UPROPERTY(BlueprintReadOnly, Category = "Pickup")
	TSubclassOf<AShooterWeapon> WeaponType;

	// Mesh of the weapon
	UPROPERTY(VisibleAnywhere, Replicated)
	USkeletalMeshComponent* WeaponMesh;

	// Number of ammo
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category ="Pickup")
	int32 AmmoAmount;

	// Check if the weapon is already set
	UPROPERTY(Replicated)
	bool bWeaponSet;

	void PickupOnTouch(class AShooterCharacter* Pawn) override;

	virtual void GivePickupTo(class AShooterCharacter* Pawn) override;

	// Add weapon to player inventory
	void AddWeapon(class AShooterCharacter* Pawn);

	// [Server] Destroy the pickup
	void DestroyPickup();

	
};
