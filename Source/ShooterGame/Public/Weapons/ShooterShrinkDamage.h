// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Weapons/ShooterDamageType.h"
#include "ShooterShrinkDamage.generated.h"

/**
 * 
 */
UCLASS(const, Blueprintable, BlueprintType)
class SHOOTERGAME_API UShooterShrinkDamage : public UShooterDamageType
{
	GENERATED_UCLASS_BODY()
	
};
