// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Weapons/ShooterDamageType.h"
#include "ShooterFreezeDamage.generated.h"

/**
 * 
 */
UCLASS(const, Blueprintable, BlueprintType)
class SHOOTERGAME_API UShooterFreezeDamage : public UShooterDamageType
{
	GENERATED_UCLASS_BODY()
	
};
