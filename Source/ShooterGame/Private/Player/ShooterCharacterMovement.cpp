// Copyright Epic Games, Inc. All Rights Reserved.

#include "ShooterGame.h"
#include "Player/ShooterCharacterMovement.h"

//----------------------------------------------------------------------//
// UPawnMovementComponent
//----------------------------------------------------------------------//
UShooterCharacterMovement::UShooterCharacterMovement(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{

	bInJetpack = false;
	CurrentFuel = MaxFuel;
	AirControl = 0.5f;
}

void UShooterCharacterMovement::OnMovementModeChanged(EMovementMode PreviousMovementMode, uint8 PreviousCustomMode)
{
	Super::OnMovementModeChanged(PreviousMovementMode, PreviousCustomMode);

	if(MovementMode == EMovementMode::MOVE_Custom && CustomMovementMode == ECustomMovementMode::CUSTOM_Jetpack)
	{
		bInJetpack = true;
	}
	else
	{
		bInJetpack = false;
	}
}

void UShooterCharacterMovement::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// Decrease the remaining fuel while flying and increase it while on ground
	if(InJetpack())
	{
		CurrentFuel = FMath::Max(0.f, CurrentFuel - DeltaTime);
	}
	else if(IsWalking())
	{
		CurrentFuel = FMath::Min(MaxFuel, CurrentFuel + DeltaTime);
	}

}

void UShooterCharacterMovement::PhysCustom(float DeltaTime, int32 Iterations)
{
	Super::PhysCustom(DeltaTime, Iterations);

	switch(CustomMovementMode)
	{
		case CUSTOM_Jetpack:
			PhysJetpack(DeltaTime, Iterations);
			break;
		default:
			break;
	}
}

void UShooterCharacterMovement::PhysJetpack(float DeltaTime, int32 Iterations)
{

	if(CurrentFuel <= 0)
	{
		SetMovementMode(EMovementMode::MOVE_Falling);
	}
	else
	{
		PhysFalling(DeltaTime, Iterations);
		AddForce(FVector(0.f, 0.f, JetpackImpulseForce * DeltaTime * 60.f));
	}
}

bool UShooterCharacterMovement::InJetpack() const
{
	return bInJetpack;
}

float UShooterCharacterMovement::GetCurrentFuel() const
{
	return CurrentFuel;
}

float UShooterCharacterMovement::GetMaxFuel() const
{
	return MaxFuel;
}

float UShooterCharacterMovement::GetMaxSpeed() const
{
	float MaxSpeed = Super::GetMaxSpeed();

	const AShooterCharacter* ShooterCharacterOwner = Cast<AShooterCharacter>(PawnOwner);
	if (ShooterCharacterOwner)
	{
		if (ShooterCharacterOwner->IsTargeting())
		{
			MaxSpeed *= ShooterCharacterOwner->GetTargetingSpeedModifier();
		}
		if (ShooterCharacterOwner->IsRunning())
		{
			MaxSpeed *= ShooterCharacterOwner->GetRunningSpeedModifier();
		}
	}

	return MaxSpeed;
}


void UShooterCharacterMovement::PerformMovement(float DeltaSeconds)
{
	ExecuteSkills(DeltaSeconds);
	Super::PerformMovement(DeltaSeconds);
}

void UShooterCharacterMovement::ExecuteSkills(float DeltaTime)
{
	if (CharacterOwner)
	{
		
		// Teleport
		if (bPressedTeleport) // bPressedTeleport is set in both the client(directly from the character input) and the server(via compressed flags)
		{
			bPressedTeleport = false;

			FHitResult HitResult;
			CharacterOwner->SetActorLocation(CharacterOwner->GetActorLocation() + CharacterOwner->GetActorForwardVector() * TeleportDistance, true, &HitResult, ETeleportType::TeleportPhysics);
		}

		// Jetpack
		if(bPressedJetpack)
		{
			if(!(MovementMode == EMovementMode::MOVE_Custom && CustomMovementMode == ECustomMovementMode::CUSTOM_Jetpack))
				SetMovementMode(EMovementMode::MOVE_Custom, ECustomMovementMode::CUSTOM_Jetpack);
		}
		else
		{
			if (MovementMode == EMovementMode::MOVE_Custom && CustomMovementMode == ECustomMovementMode::CUSTOM_Jetpack)
				SetMovementMode(EMovementMode::MOVE_Falling);
		}
	}
}

void UShooterCharacterMovement::UpdateFromCompressedFlags(uint8 Flags)
{
	Super::UpdateFromCompressedFlags(Flags);

	// Update state variables using compressed flag set in FCustomSavedMove
	bPressedTeleport = ((Flags & FSavedMove_Character::FLAG_Custom_0) != 0);
	bPressedJetpack = ((Flags & FSavedMove_Character::FLAG_Custom_1) != 0);

}

class FNetworkPredictionData_Client* UShooterCharacterMovement::GetPredictionData_Client() const
{
	// Initialize an instance of FCustomPredictionData
	if (!ClientPredictionData)
	{
		UShooterCharacterMovement* MutableThis = const_cast<UShooterCharacterMovement*>(this);

		MutableThis->ClientPredictionData = new class FCustomNetworkPredictionData_Client(*this);
		MutableThis->ClientPredictionData->MaxSmoothNetUpdateDist = 92.f; // 2X character capsule radius
		MutableThis->ClientPredictionData->NoSmoothNetUpdateDist = 140.f;
	}

	return ClientPredictionData;
}

void FCustomSavedMove::Clear()
{
	Super::Clear();
	bPressedTeleport = false;
	bPressedJetpack = false;
}

uint8 FCustomSavedMove::GetCompressedFlags() const
{
	uint8 Result = 0;

	if (bPressedJump)
	{
		Result |= FLAG_JumpPressed;
	}

	if (bWantsToCrouch)
	{
		Result |= FLAG_WantsToCrouch;
	}
	
	// Add the state variable as single bits in the compressed flags that will later be extracted from the server
	if (bPressedTeleport)
	{
		Result |= FLAG_Custom_0;
	}

	if (bPressedJetpack)
	{
		Result |= FLAG_Custom_1;
	}

	return Result;
}

bool FCustomSavedMove::CanCombineWith(const FSavedMovePtr& NewMove, ACharacter* Character, float MaxDelta) const
{
	// If two subsequent SavedMove are of the same tipe, it is possible to combine them
	if (bPressedJetpack != ((FCustomSavedMove*)&NewMove)->bPressedJetpack)
	{
		return false;
	}

	if (bPressedTeleport != ((FCustomSavedMove*)&NewMove)->bPressedTeleport)
	{
		return false;
	}

	return Super::CanCombineWith(NewMove, Character, MaxDelta);
}

void FCustomSavedMove::SetMoveFor(ACharacter* Character, float InDeltaTime, FVector const& NewAccel, class FNetworkPredictionData_Client_Character& ClientData)
{
	Super::SetMoveFor(Character, InDeltaTime, NewAccel, ClientData);

	// Saved Move <--- Character Movement Data
	UShooterCharacterMovement* CharacterMovement = Cast<UShooterCharacterMovement>(Character->GetCharacterMovement());

	if (CharacterMovement)
	{
		bPressedTeleport = CharacterMovement->bPressedTeleport;
		bPressedJetpack = CharacterMovement->bPressedJetpack;
	}
}

void FCustomSavedMove::PrepMoveFor(class ACharacter* Character)
{
	Super::PrepMoveFor(Character);

	// Saved Move ---> Character Movement Data 
	UShooterCharacterMovement* CharacterMovement = Cast<UShooterCharacterMovement>(Character->GetCharacterMovement());

	if (CharacterMovement)
	{
		CharacterMovement->bPressedTeleport = bPressedTeleport;
		CharacterMovement->bPressedJetpack = bPressedJetpack;
	}
}

FSavedMovePtr FCustomNetworkPredictionData_Client::AllocateNewMove()
{

	return FSavedMovePtr(new FCustomSavedMove());

}