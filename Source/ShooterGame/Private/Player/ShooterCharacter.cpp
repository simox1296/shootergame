// Copyright Epic Games, Inc. All Rights Reserved.

#include "ShooterGame.h"
#include "Weapons/ShooterWeapon.h"
#include "Weapons/ShooterDamageType.h"
#include "Weapons/ShooterFreezeDamage.h"
#include "Weapons/ShooterShrinkDamage.h"
#include "UI/ShooterHUD.h"
#include "Online/ShooterPlayerState.h"
#include "Animation/AnimMontage.h"
#include "Animation/AnimInstance.h"
#include "Sound/SoundNodeLocalPlayer.h"
#include "AudioThread.h"
#include "Pickups/ShooterPickup_Weapon.h"

static int32 NetVisualizeRelevancyTestPoints = 0;
FAutoConsoleVariableRef CVarNetVisualizeRelevancyTestPoints(
	TEXT("p.NetVisualizeRelevancyTestPoints"),
	NetVisualizeRelevancyTestPoints,
	TEXT("")
	TEXT("0: Disable, 1: Enable"),
	ECVF_Cheat);


static int32 NetEnablePauseRelevancy = 1;
FAutoConsoleVariableRef CVarNetEnablePauseRelevancy(
	TEXT("p.NetEnablePauseRelevancy"),
	NetEnablePauseRelevancy,
	TEXT("")
	TEXT("0: Disable, 1: Enable"),
	ECVF_Cheat);

FOnShooterCharacterEquipWeapon AShooterCharacter::NotifyEquipWeapon;
FOnShooterCharacterUnEquipWeapon AShooterCharacter::NotifyUnEquipWeapon;

AShooterCharacter::AShooterCharacter(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer.SetDefaultSubobjectClass<UShooterCharacterMovement>(ACharacter::CharacterMovementComponentName))
{
	Mesh1P = ObjectInitializer.CreateDefaultSubobject<USkeletalMeshComponent>(this, TEXT("PawnMesh1P"));
	Mesh1P->SetupAttachment(GetCapsuleComponent());
	Mesh1P->bOnlyOwnerSee = true;
	Mesh1P->bOwnerNoSee = false;
	Mesh1P->bCastDynamicShadow = false;
	Mesh1P->bReceivesDecals = false;
	Mesh1P->VisibilityBasedAnimTickOption = EVisibilityBasedAnimTickOption::OnlyTickPoseWhenRendered;
	Mesh1P->PrimaryComponentTick.TickGroup = TG_PrePhysics;
	Mesh1P->SetCollisionObjectType(ECC_Pawn);
	Mesh1P->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	Mesh1P->SetCollisionResponseToAllChannels(ECR_Ignore);

	GetMesh()->bOnlyOwnerSee = false;
	GetMesh()->bOwnerNoSee = true;
	GetMesh()->bReceivesDecals = false;
	GetMesh()->SetCollisionObjectType(ECC_Pawn);
	GetMesh()->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	GetMesh()->SetCollisionResponseToChannel(COLLISION_WEAPON, ECR_Block);
	GetMesh()->SetCollisionResponseToChannel(COLLISION_PROJECTILE, ECR_Block);
	GetMesh()->SetCollisionResponseToChannel(ECC_Visibility, ECR_Block);

	GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_Camera, ECR_Ignore);
	GetCapsuleComponent()->SetCollisionResponseToChannel(COLLISION_PROJECTILE, ECR_Block);
	GetCapsuleComponent()->SetCollisionResponseToChannel(COLLISION_WEAPON, ECR_Ignore);
	GetCapsuleComponent()->SetNotifyRigidBodyCollision(true);

	ShooterCharacterMovement = Cast<UShooterCharacterMovement>(GetCharacterMovement());

	FreezePSComponent = ObjectInitializer.CreateDefaultSubobject<UParticleSystemComponent>(this, TEXT("FreezePS"));
	FreezePSComponent->SetupAttachment(GetMesh());
	FreezePSComponent->bAutoActivate = false;

	TargetingSpeedModifier = 0.5f;
	bIsTargeting = false;
	RunningSpeedModifier = 1.5f;
	bWantsToRun = false;
	bWantsToFire = false;
	LowHealthPercentage = 0.5f;

	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	bCanTeleport = true;
	bCanRewind = true;
	bCanJetpack = true;
	bCanWallJump = true;
	bCanWallRun = true;

	bInRewind = false;
	bInWallRun = false;
	bIsFreezing = false;
	bIsShrinked = false;
	bInShrinkingProcess = false;

	bNotifyHitCalled = false;
	bNewWall = false;
	bOnWall = false;
	
	MaxLocations = (int32)(RewindTimeSpan / StoreLocationTimeOffset);
	ShrinkRemainingTime = 0.f;
	DefaultEyeHeight = BaseEyeHeight;

}

void AShooterCharacter::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	if (GetLocalRole() == ROLE_Authority)
	{
		Health = GetMaxHealth();

		// Needs to happen after character is added to repgraph
		GetWorldTimerManager().SetTimerForNextTick(this, &AShooterCharacter::SpawnDefaultInventory);

		// Begin storing locations of the player
		PastLocations.Empty();
		GetWorld()->GetTimerManager().SetTimer(StoreLocationsHandle, this, &AShooterCharacter::StoreCurrentLocation, StoreLocationTimeOffset, true);

	}

	// set initial mesh visibility (3rd person view)
	UpdatePawnMeshes();

	// create material instance for setting team colors (3rd person view)
	for (int32 iMat = 0; iMat < GetMesh()->GetNumMaterials(); iMat++)
	{
		MeshMIDs.Add(GetMesh()->CreateAndSetMaterialInstanceDynamic(iMat));
	}

	// play respawn effects
	if (GetNetMode() != NM_DedicatedServer)
	{
		if (RespawnFX)
		{
			UGameplayStatics::SpawnEmitterAtLocation(this, RespawnFX, GetActorLocation(), GetActorRotation());
		}

		if (RespawnSound)
		{
			UGameplayStatics::PlaySoundAtLocation(this, RespawnSound, GetActorLocation());
		}
	}

}

void AShooterCharacter::Destroyed()
{
	Super::Destroyed();
	DestroyInventory();
}

void AShooterCharacter::PawnClientRestart()
{
	Super::PawnClientRestart();

	// switch mesh to 1st person view
	UpdatePawnMeshes();

	// reattach weapon if needed
	SetCurrentWeapon(CurrentWeapon);

	// set team colors for 1st person view
	UMaterialInstanceDynamic* Mesh1PMID = Mesh1P->CreateAndSetMaterialInstanceDynamic(0);
	UpdateTeamColors(Mesh1PMID);

}

void AShooterCharacter::PossessedBy(class AController* InController)
{
	Super::PossessedBy(InController);

	// [server] as soon as PlayerState is assigned, set team colors of this pawn for local player
	UpdateTeamColorsAllMIDs();
}

void AShooterCharacter::OnRep_PlayerState()
{
	Super::OnRep_PlayerState();

	// [client] as soon as PlayerState is assigned, set team colors of this pawn for local player
	if (GetPlayerState() != NULL)
	{
		UpdateTeamColorsAllMIDs();
	}
}

FRotator AShooterCharacter::GetAimOffsets() const
{
	const FVector AimDirWS = GetBaseAimRotation().Vector();
	const FVector AimDirLS = ActorToWorld().InverseTransformVectorNoScale(AimDirWS);
	const FRotator AimRotLS = AimDirLS.Rotation();

	return AimRotLS;
}

bool AShooterCharacter::IsEnemyFor(AController* TestPC) const
{
	if (TestPC == Controller || TestPC == NULL)
	{
		return false;
	}

	AShooterPlayerState* TestPlayerState = Cast<AShooterPlayerState>(TestPC->PlayerState);
	AShooterPlayerState* MyPlayerState = Cast<AShooterPlayerState>(GetPlayerState());

	bool bIsEnemy = true;
	if (GetWorld()->GetGameState())
	{
		const AShooterGameMode* DefGame = GetWorld()->GetGameState()->GetDefaultGameMode<AShooterGameMode>();
		if (DefGame && MyPlayerState && TestPlayerState)
		{
			bIsEnemy = DefGame->CanDealDamage(TestPlayerState, MyPlayerState);
		}
	}

	return bIsEnemy;
}

//////////////////////////////////////////////////////////////////////////
// Meshes

void AShooterCharacter::UpdatePawnMeshes()
{
	bool const bFirstPerson = IsFirstPerson();

	Mesh1P->VisibilityBasedAnimTickOption = !bFirstPerson ? EVisibilityBasedAnimTickOption::OnlyTickPoseWhenRendered : EVisibilityBasedAnimTickOption::AlwaysTickPoseAndRefreshBones;
	Mesh1P->SetOwnerNoSee(!bFirstPerson);

	GetMesh()->VisibilityBasedAnimTickOption = bFirstPerson ? EVisibilityBasedAnimTickOption::OnlyTickPoseWhenRendered : EVisibilityBasedAnimTickOption::AlwaysTickPoseAndRefreshBones;
	GetMesh()->SetOwnerNoSee(bFirstPerson);
}

void AShooterCharacter::UpdateTeamColors(UMaterialInstanceDynamic* UseMID)
{
	if (UseMID)
	{
		AShooterPlayerState* MyPlayerState = Cast<AShooterPlayerState>(GetPlayerState());
		if (MyPlayerState != NULL)
		{
			float MaterialParam = (float)MyPlayerState->GetTeamNum();
			UseMID->SetScalarParameterValue(TEXT("Team Color Index"), MaterialParam);
		}
	}
}

void AShooterCharacter::OnCameraUpdate(const FVector& CameraLocation, const FRotator& CameraRotation)
{
	USkeletalMeshComponent* DefMesh1P = Cast<USkeletalMeshComponent>(GetClass()->GetDefaultSubobjectByName(TEXT("PawnMesh1P")));
	const FMatrix DefMeshLS = FRotationTranslationMatrix(DefMesh1P->GetRelativeRotation(), bIsShrinked? DefMesh1P->GetRelativeLocation() + ShrinkMeshOffset : DefMesh1P->GetRelativeLocation());
	const FMatrix LocalToWorld = ActorToWorld().ToMatrixWithScale();

	// Mesh rotating code expect uniform scale in LocalToWorld matrix

	const FRotator RotCameraPitch(CameraRotation.Pitch, 0.0f, 0.0f);
	const FRotator RotCameraYaw(0.0f, CameraRotation.Yaw, 0.0f);

	const FMatrix LeveledCameraLS = FRotationTranslationMatrix(RotCameraYaw, CameraLocation) * LocalToWorld.Inverse();
	const FMatrix PitchedCameraLS = FRotationMatrix(RotCameraPitch) * LeveledCameraLS;
	const FMatrix MeshRelativeToCamera = DefMeshLS * LeveledCameraLS.Inverse();
	const FMatrix PitchedMesh = MeshRelativeToCamera * PitchedCameraLS;

	Mesh1P->SetRelativeLocationAndRotation(PitchedMesh.GetOrigin(), PitchedMesh.Rotator());
}


//////////////////////////////////////////////////////////////////////////
// Damage & death


void AShooterCharacter::FellOutOfWorld(const class UDamageType& dmgType)
{
	Die(Health, FDamageEvent(dmgType.GetClass()), NULL, NULL);
}

void AShooterCharacter::Suicide()
{
	KilledBy(this);
}

void AShooterCharacter::KilledBy(APawn* EventInstigator)
{
	if (GetLocalRole() == ROLE_Authority && !bIsDying)
	{
		AController* Killer = NULL;
		if (EventInstigator != NULL)
		{
			Killer = EventInstigator->Controller;
			LastHitBy = NULL;
		}

		Die(Health, FDamageEvent(UDamageType::StaticClass()), Killer, NULL);
	}
}


float AShooterCharacter::TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, class AActor* DamageCauser)
{
	AShooterPlayerController* MyPC = Cast<AShooterPlayerController>(Controller);
	if (MyPC && MyPC->HasGodMode())
	{
		return 0.f;
	}

	if (Health <= 0.f)
	{
		return 0.f;
	}
	
	UObject *DamageObj = DamageEvent.DamageTypeClass->GetDefaultObject();

	// Handle freezing damage
	if(DamageObj->IsA(UShooterFreezeDamage::StaticClass())){
		ServerFreeze();

		return 0.f;
	}

	// Handle shrinking damage
	if(DamageObj->IsA(UShooterShrinkDamage::StaticClass())){
		ServerShrink();

		return 0.f;
	}


	// Modify based on game rules.
	AShooterGameMode* const Game = GetWorld()->GetAuthGameMode<AShooterGameMode>();
	Damage = Game ? Game->ModifyDamage(Damage, this, DamageEvent, EventInstigator, DamageCauser) : 0.f;

	const float ActualDamage = Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);
	if (ActualDamage > 0.f)
	{
		Health -= ActualDamage;
		if (Health <= 0)
		{
			Die(ActualDamage, DamageEvent, EventInstigator, DamageCauser);
		}
		else
		{
			PlayHit(ActualDamage, DamageEvent, EventInstigator ? EventInstigator->GetPawn() : NULL, DamageCauser);
		}

		MakeNoise(1.0f, EventInstigator ? EventInstigator->GetPawn() : this);
	}

	return ActualDamage;
	
}


bool AShooterCharacter::CanDie(float KillingDamage, FDamageEvent const& DamageEvent, AController* Killer, AActor* DamageCauser) const
{
	if (bIsDying										// already dying
		|| IsPendingKill()								// already destroyed
		|| GetLocalRole() != ROLE_Authority				// not authority
		|| GetWorld()->GetAuthGameMode<AShooterGameMode>() == NULL
		|| GetWorld()->GetAuthGameMode<AShooterGameMode>()->GetMatchState() == MatchState::LeavingMap)	// level transition occurring
	{
		return false;
	}

	return true;
}


bool AShooterCharacter::Die(float KillingDamage, FDamageEvent const& DamageEvent, AController* Killer, AActor* DamageCauser)
{
	if (!CanDie(KillingDamage, DamageEvent, Killer, DamageCauser))
	{
		return false;
	}

	Health = FMath::Min(0.0f, Health);

	// if this is an environmental death then refer to the previous killer so that they receive credit (knocked into lava pits, etc)
	UDamageType const* const DamageType = DamageEvent.DamageTypeClass ? DamageEvent.DamageTypeClass->GetDefaultObject<UDamageType>() : GetDefault<UDamageType>();
	Killer = GetDamageInstigator(Killer, *DamageType);

	AController* const KilledPlayer = (Controller != NULL) ? Controller : Cast<AController>(GetOwner());
	GetWorld()->GetAuthGameMode<AShooterGameMode>()->Killed(Killer, KilledPlayer, this, DamageType);

	NetUpdateFrequency = GetDefault<AShooterCharacter>()->NetUpdateFrequency;
	GetCharacterMovement()->ForceReplicationUpdate();

	SpawnWeaponPickup();

	OnDeath(KillingDamage, DamageEvent, Killer ? Killer->GetPawn() : NULL, DamageCauser);
	return true;
}


void AShooterCharacter::OnDeath(float KillingDamage, struct FDamageEvent const& DamageEvent, class APawn* PawnInstigator, class AActor* DamageCauser)
{
	if (bIsDying)
	{
		return;
	}

	SetReplicatingMovement(false);
	TearOff();
	bIsDying = true;

	if (GetLocalRole() == ROLE_Authority)
	{
		ReplicateHit(KillingDamage, DamageEvent, PawnInstigator, DamageCauser, true);

		// play the force feedback effect on the client player controller
		AShooterPlayerController* PC = Cast<AShooterPlayerController>(Controller);
		if (PC && DamageEvent.DamageTypeClass)
		{
			UShooterDamageType *DamageType = Cast<UShooterDamageType>(DamageEvent.DamageTypeClass->GetDefaultObject());
			if (DamageType && DamageType->KilledForceFeedback && PC->IsVibrationEnabled())
			{
				FForceFeedbackParameters FFParams;
				FFParams.Tag = "Damage";
				PC->ClientPlayForceFeedback(DamageType->KilledForceFeedback, FFParams);
			}
		}
	}

	// cannot use IsLocallyControlled here, because even local client's controller may be NULL here
	if (GetNetMode() != NM_DedicatedServer && DeathSound && Mesh1P && Mesh1P->IsVisible())
	{
		UGameplayStatics::PlaySoundAtLocation(this, DeathSound, GetActorLocation());
	}

	// remove all weapons
	DestroyInventory();

	// switch back to 3rd person view
	UpdatePawnMeshes();

	DetachFromControllerPendingDestroy();
	StopAllAnimMontages();

	if (LowHealthWarningPlayer && LowHealthWarningPlayer->IsPlaying())
	{
		LowHealthWarningPlayer->Stop();
	}

	if (RunLoopAC)
	{
		RunLoopAC->Stop();
	}

	if (GetMesh())
	{
		static FName CollisionProfileName(TEXT("Ragdoll"));
		GetMesh()->SetCollisionProfileName(CollisionProfileName);
	}
	SetActorEnableCollision(true);

	// Death anim
	float DeathAnimDuration = PlayAnimMontage(DeathAnim);

	
	// Ragdoll
	if (DeathAnimDuration > 0.f)
	{
		// Trigger ragdoll a little before the animation early so the character doesn't
		// blend back to its normal position.
		const float TriggerRagdollTime = DeathAnimDuration - 0.7f;

		// Enable blend physics so the bones are properly blending against the montage.
		GetMesh()->bBlendPhysics = true;

		// Use a local timer handle as we don't need to store it for later but we don't need to look for something to clear
		FTimerHandle TimerHandle;
		GetWorldTimerManager().SetTimer(TimerHandle, this, &AShooterCharacter::SetRagdollPhysics, FMath::Max(0.1f, TriggerRagdollTime), false);
	}
	else
	{
		SetRagdollPhysics();
	}

	// disable collisions on capsule
	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	GetCapsuleComponent()->SetCollisionResponseToAllChannels(ECR_Ignore);
}

void AShooterCharacter::PlayHit(float DamageTaken, struct FDamageEvent const& DamageEvent, class APawn* PawnInstigator, class AActor* DamageCauser)
{
	if (GetLocalRole() == ROLE_Authority)
	{
		ReplicateHit(DamageTaken, DamageEvent, PawnInstigator, DamageCauser, false);

		// play the force feedback effect on the client player controller
		AShooterPlayerController* PC = Cast<AShooterPlayerController>(Controller);
		if (PC && DamageEvent.DamageTypeClass)
		{
			UShooterDamageType *DamageType = Cast<UShooterDamageType>(DamageEvent.DamageTypeClass->GetDefaultObject());
			if (DamageType && DamageType->HitForceFeedback && PC->IsVibrationEnabled())
			{
				FForceFeedbackParameters FFParams;
				FFParams.Tag = "Damage";
				PC->ClientPlayForceFeedback(DamageType->HitForceFeedback, FFParams);
			}
		}
	}

	if (DamageTaken > 0.f)
	{
		ApplyDamageMomentum(DamageTaken, DamageEvent, PawnInstigator, DamageCauser);
	}

	AShooterPlayerController* MyPC = Cast<AShooterPlayerController>(Controller);
	AShooterHUD* MyHUD = MyPC ? Cast<AShooterHUD>(MyPC->GetHUD()) : NULL;
	if (MyHUD)
	{
		MyHUD->NotifyWeaponHit(DamageTaken, DamageEvent, PawnInstigator);
	}

	if (PawnInstigator && PawnInstigator != this && PawnInstigator->IsLocallyControlled())
	{
		AShooterPlayerController* InstigatorPC = Cast<AShooterPlayerController>(PawnInstigator->Controller);
		AShooterHUD* InstigatorHUD = InstigatorPC ? Cast<AShooterHUD>(InstigatorPC->GetHUD()) : NULL;
		if (InstigatorHUD)
		{
			InstigatorHUD->NotifyEnemyHit();
		}
	}
}


void AShooterCharacter::SetRagdollPhysics()
{
	bool bInRagdoll = false;

	if (IsPendingKill())
	{
		bInRagdoll = false;
	}
	else if (!GetMesh() || !GetMesh()->GetPhysicsAsset())
	{
		bInRagdoll = false;
	}
	else if( bIsShrinked ) // Avoid using ragdoll when mesh is scaled
	{
		bInRagdoll;
	}
	else
	{
		// initialize physics/etc
		GetMesh()->SetSimulatePhysics(true);
		GetMesh()->WakeAllRigidBodies();
		GetMesh()->bBlendPhysics = true;

		bInRagdoll = true;
	}

	GetCharacterMovement()->StopMovementImmediately();
	GetCharacterMovement()->DisableMovement();
	GetCharacterMovement()->SetComponentTickEnabled(false);

	if (!bInRagdoll)
	{
		// hide and set short lifespan
		TurnOff();
		SetActorHiddenInGame(true);
		SetLifeSpan(1.0f);
	}
	else
	{
		SetLifeSpan(10.0f);
	}
}



void AShooterCharacter::ReplicateHit(float Damage, struct FDamageEvent const& DamageEvent, class APawn* PawnInstigator, class AActor* DamageCauser, bool bKilled)
{
	const float TimeoutTime = GetWorld()->GetTimeSeconds() + 0.5f;

	FDamageEvent const& LastDamageEvent = LastTakeHitInfo.GetDamageEvent();
	if ((PawnInstigator == LastTakeHitInfo.PawnInstigator.Get()) && (LastDamageEvent.DamageTypeClass == LastTakeHitInfo.DamageTypeClass) && (LastTakeHitTimeTimeout == TimeoutTime))
	{
		// same frame damage
		if (bKilled && LastTakeHitInfo.bKilled)
		{
			// Redundant death take hit, just ignore it
			return;
		}

		// otherwise, accumulate damage done this frame
		Damage += LastTakeHitInfo.ActualDamage;
	}

	LastTakeHitInfo.ActualDamage = Damage;
	LastTakeHitInfo.PawnInstigator = Cast<AShooterCharacter>(PawnInstigator);
	LastTakeHitInfo.DamageCauser = DamageCauser;
	LastTakeHitInfo.SetDamageEvent(DamageEvent);
	LastTakeHitInfo.bKilled = bKilled;
	LastTakeHitInfo.EnsureReplication();

	LastTakeHitTimeTimeout = TimeoutTime;
}

void AShooterCharacter::OnRep_LastTakeHitInfo()
{
	if (LastTakeHitInfo.bKilled)
	{
		OnDeath(LastTakeHitInfo.ActualDamage, LastTakeHitInfo.GetDamageEvent(), LastTakeHitInfo.PawnInstigator.Get(), LastTakeHitInfo.DamageCauser.Get());
	}
	else
	{
		PlayHit(LastTakeHitInfo.ActualDamage, LastTakeHitInfo.GetDamageEvent(), LastTakeHitInfo.PawnInstigator.Get(), LastTakeHitInfo.DamageCauser.Get());
	}
}

//Pawn::PlayDying sets this lifespan, but when that function is called on client, dead pawn's role is still SimulatedProxy despite bTearOff being true. 
void AShooterCharacter::TornOff()
{
	SetLifeSpan(25.f);
}

bool AShooterCharacter::IsMoving()
{
	return FMath::Abs(GetLastMovementInputVector().Size()) > 0.f;
}

//////////////////////////////////////////////////////////////////////////
// Inventory

void AShooterCharacter::SpawnDefaultInventory()
{
	if (GetLocalRole() < ROLE_Authority)
	{
		return;
	}

	int32 NumWeaponClasses = DefaultInventoryClasses.Num();
	for (int32 i = 0; i < NumWeaponClasses; i++)
	{
		if (DefaultInventoryClasses[i])
		{
			FActorSpawnParameters SpawnInfo;
			SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			AShooterWeapon* NewWeapon = GetWorld()->SpawnActor<AShooterWeapon>(DefaultInventoryClasses[i], SpawnInfo);
			AddWeapon(NewWeapon);
		}
	}

	// equip first weapon in inventory
	if (Inventory.Num() > 0)
	{
		EquipWeapon(Inventory[0]);
	}
}

void AShooterCharacter::DestroyInventory()
{
	if (GetLocalRole() < ROLE_Authority)
	{
		return;
	}

	// remove all weapons from inventory and destroy them
	for (int32 i = Inventory.Num() - 1; i >= 0; i--)
	{
		AShooterWeapon* Weapon = Inventory[i];
		if (Weapon)
		{
			RemoveWeapon(Weapon);
			Weapon->Destroy();
		}
	}
}

void AShooterCharacter::AddWeapon(AShooterWeapon* Weapon)
{
	if (Weapon && GetLocalRole() == ROLE_Authority)
	{
		Weapon->OnEnterInventory(this);
		Inventory.AddUnique(Weapon);
	}
}

void AShooterCharacter::RemoveWeapon(AShooterWeapon* Weapon)
{
	if (Weapon && GetLocalRole() == ROLE_Authority)
	{
		Weapon->OnLeaveInventory();
		Inventory.RemoveSingle(Weapon);
	}
}

AShooterWeapon* AShooterCharacter::FindWeapon(TSubclassOf<AShooterWeapon> WeaponClass)
{
	for (int32 i = 0; i < Inventory.Num(); i++)
	{
		if (Inventory[i] && Inventory[i]->IsA(WeaponClass))
		{
			return Inventory[i];
		}
	}

	return NULL;
}

void AShooterCharacter::EquipWeapon(AShooterWeapon* Weapon)
{
	if (Weapon)
	{
		if (GetLocalRole() == ROLE_Authority)
		{
			SetCurrentWeapon(Weapon, CurrentWeapon);
		}
		else
		{
			ServerEquipWeapon(Weapon);
		}
	}
}

bool AShooterCharacter::ServerEquipWeapon_Validate(AShooterWeapon* Weapon)
{
	return true;
}

void AShooterCharacter::ServerEquipWeapon_Implementation(AShooterWeapon* Weapon)
{
	EquipWeapon(Weapon);
}

void AShooterCharacter::OnRep_CurrentWeapon(AShooterWeapon* LastWeapon)
{
	SetCurrentWeapon(CurrentWeapon, LastWeapon);
}

void AShooterCharacter::SetCurrentWeapon(AShooterWeapon* NewWeapon, AShooterWeapon* LastWeapon)
{
	AShooterWeapon* LocalLastWeapon = nullptr;

	if (LastWeapon != NULL)
	{
		LocalLastWeapon = LastWeapon;
	}
	else if (NewWeapon != CurrentWeapon)
	{
		LocalLastWeapon = CurrentWeapon;
	}

	// unequip previous
	if (LocalLastWeapon)
	{
		// Reenable collision, if it was deactivated
		if(bIsShrinked)
		{
			LocalLastWeapon->SetActorEnableCollision(true);
		}
			

		LocalLastWeapon->OnUnEquip();
	}

	CurrentWeapon = NewWeapon;

	// equip new one
	if (NewWeapon)
	{
		// Disable collision if the character is shrinked
		if(bIsShrinked)
		{
			LocalLastWeapon->SetActorEnableCollision(true);
		}

		NewWeapon->SetOwningPawn(this);	// Make sure weapon's MyPawn is pointing back to us. During replication, we can't guarantee APawn::CurrentWeapon will rep after AWeapon::MyPawn!

		NewWeapon->OnEquip(LastWeapon);
	}
}


//////////////////////////////////////////////////////////////////////////
// Weapon usage

void AShooterCharacter::StartWeaponFire()
{
	if (!bWantsToFire)
	{
		bWantsToFire = true;
		if (CurrentWeapon)
		{
			CurrentWeapon->StartFire();
		}
	}
}

void AShooterCharacter::StopWeaponFire()
{
	if (bWantsToFire)
	{
		bWantsToFire = false;
		if (CurrentWeapon)
		{
			CurrentWeapon->StopFire();
		}
	}
}

bool AShooterCharacter::CanFire() const
{
	return IsAlive();
}

bool AShooterCharacter::CanReload() const
{
	return true;
}

void AShooterCharacter::SetTargeting(bool bNewTargeting)
{
	bIsTargeting = bNewTargeting;

	if (TargetingSound)
	{
		UGameplayStatics::SpawnSoundAttached(TargetingSound, GetRootComponent());
	}

	if (GetLocalRole() < ROLE_Authority)
	{
		ServerSetTargeting(bNewTargeting);
	}
}

bool AShooterCharacter::ServerSetTargeting_Validate(bool bNewTargeting)
{
	return true;
}

void AShooterCharacter::ServerSetTargeting_Implementation(bool bNewTargeting)
{
	SetTargeting(bNewTargeting);
}

//////////////////////////////////////////////////////////////////////////
// Movement

void AShooterCharacter::SetRunning(bool bNewRunning, bool bToggle)
{
	bWantsToRun = bNewRunning;
	bWantsToRunToggled = bNewRunning && bToggle;

	if (GetLocalRole() < ROLE_Authority)
	{
		ServerSetRunning(bNewRunning, bToggle);
	}
}

bool AShooterCharacter::ServerSetRunning_Validate(bool bNewRunning, bool bToggle)
{
	return true;
}

void AShooterCharacter::ServerSetRunning_Implementation(bool bNewRunning, bool bToggle)
{
	SetRunning(bNewRunning, bToggle);
}

void AShooterCharacter::UpdateRunSounds()
{
	const bool bIsRunSoundPlaying = RunLoopAC != nullptr && RunLoopAC->IsActive();
	const bool bWantsRunSoundPlaying = IsRunning() && IsMoving();

	// Don't bother playing the sounds unless we're running and moving.
	if (!bIsRunSoundPlaying && bWantsRunSoundPlaying)
	{
		if (RunLoopAC != nullptr)
		{
			RunLoopAC->Play();
		}
		else if (RunLoopSound != nullptr)
		{
			RunLoopAC = UGameplayStatics::SpawnSoundAttached(RunLoopSound, GetRootComponent());
			if (RunLoopAC != nullptr)
			{
				RunLoopAC->bAutoDestroy = false;
			}
		}
	}
	else if (bIsRunSoundPlaying && !bWantsRunSoundPlaying)
	{
		RunLoopAC->Stop();
		if (RunStopSound != nullptr)
		{
			UGameplayStatics::SpawnSoundAttached(RunStopSound, GetRootComponent());
		}
	}
}

//////////////////////////////////////////////////////////////////////////
// Animations

float AShooterCharacter::PlayAnimMontage(class UAnimMontage* AnimMontage, float InPlayRate, FName StartSectionName)
{
	USkeletalMeshComponent* UseMesh = GetPawnMesh();
	if (AnimMontage && UseMesh && UseMesh->AnimScriptInstance)
	{
		return UseMesh->AnimScriptInstance->Montage_Play(AnimMontage, InPlayRate);
	}

	return 0.0f;
}

void AShooterCharacter::StopAnimMontage(class UAnimMontage* AnimMontage)
{
	USkeletalMeshComponent* UseMesh = GetPawnMesh();
	if (AnimMontage && UseMesh && UseMesh->AnimScriptInstance &&
		UseMesh->AnimScriptInstance->Montage_IsPlaying(AnimMontage))
	{
		UseMesh->AnimScriptInstance->Montage_Stop(AnimMontage->BlendOut.GetBlendTime(), AnimMontage);
	}
}

void AShooterCharacter::StopAllAnimMontages()
{
	USkeletalMeshComponent* UseMesh = GetPawnMesh();
	if (UseMesh && UseMesh->AnimScriptInstance)
	{
		UseMesh->AnimScriptInstance->Montage_Stop(0.0f);
	}
}


//////////////////////////////////////////////////////////////////////////
// Input

void AShooterCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	check(PlayerInputComponent);
	PlayerInputComponent->BindAxis("MoveForward", this, &AShooterCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AShooterCharacter::MoveRight);
	PlayerInputComponent->BindAxis("MoveUp", this, &AShooterCharacter::MoveUp);
	PlayerInputComponent->BindAxis("Turn", this, &AShooterCharacter::Turn);
	PlayerInputComponent->BindAxis("TurnRate", this, &AShooterCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &AShooterCharacter::LookUp);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AShooterCharacter::LookUpAtRate);

	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &AShooterCharacter::OnStartFire);
	PlayerInputComponent->BindAction("Fire", IE_Released, this, &AShooterCharacter::OnStopFire);

	PlayerInputComponent->BindAction("Targeting", IE_Pressed, this, &AShooterCharacter::OnStartTargeting);
	PlayerInputComponent->BindAction("Targeting", IE_Released, this, &AShooterCharacter::OnStopTargeting);

	PlayerInputComponent->BindAction("NextWeapon", IE_Pressed, this, &AShooterCharacter::OnNextWeapon);
	PlayerInputComponent->BindAction("PrevWeapon", IE_Pressed, this, &AShooterCharacter::OnPrevWeapon);

	PlayerInputComponent->BindAction("Reload", IE_Pressed, this, &AShooterCharacter::OnReload);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &AShooterCharacter::OnStartJump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &AShooterCharacter::OnStopJump);

	PlayerInputComponent->BindAction("Run", IE_Pressed, this, &AShooterCharacter::OnStartRunning);
	PlayerInputComponent->BindAction("RunToggle", IE_Pressed, this, &AShooterCharacter::OnStartRunningToggle);
	PlayerInputComponent->BindAction("Run", IE_Released, this, &AShooterCharacter::OnStopRunning);

	PlayerInputComponent->BindAction("Rewind", IE_Pressed, this, &AShooterCharacter::OnRewind);
	PlayerInputComponent->BindAction("Teleport", IE_Pressed, this, &AShooterCharacter::OnTeleport);
}


void AShooterCharacter::MoveForward(float Val)
{
	if (Controller && Val != 0.f)
	{
		if(!bIsFreezing)
		{
			// Limit pitch when walking or falling
			const bool bLimitRotation = (GetCharacterMovement()->IsMovingOnGround() || GetCharacterMovement()->IsFalling());
			const FRotator Rotation = bLimitRotation ? GetActorRotation() : Controller->GetControlRotation();
			const FVector Direction = FRotationMatrix(Rotation).GetScaledAxis(EAxis::X);
			AddMovementInput(Direction, Val);
		}
		else
		{
			GetWorld()->GetFirstPlayerController()->PlayerCameraManager->PlayCameraShake(FreezeCameraShake);
		}
		
	}
}

void AShooterCharacter::MoveRight(float Val)
{
	if (Val != 0.f)
	{
		if(!bIsFreezing)
		{
			const FQuat Rotation = GetActorQuat();
			const FVector Direction = FQuatRotationMatrix(Rotation).GetScaledAxis(EAxis::Y);
			AddMovementInput(Direction, Val);
		}
		else
		{
			GetWorld()->GetFirstPlayerController()->PlayerCameraManager->PlayCameraShake(FreezeCameraShake);
		}
	}
}

void AShooterCharacter::MoveUp(float Val)
{
	if (Val != 0.f)
	{
		// Not when walking or falling.
		if (GetCharacterMovement()->IsMovingOnGround() || GetCharacterMovement()->IsFalling() || ShooterCharacterMovement->InJetpack())
		{
			return;
		}

		AddMovementInput(FVector::UpVector, Val);
	}
}

void AShooterCharacter::TurnAtRate(float Val)
{
	if(!bIsFreezing)
	{
		// calculate delta for this frame from the rate information
		AddControllerYawInput(Val * BaseTurnRate * GetWorld()->GetDeltaSeconds());
	}
}

void AShooterCharacter::LookUpAtRate(float Val)
{
	if(!bIsFreezing)
	{
		// calculate delta for this frame from the rate information
		AddControllerPitchInput(Val * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
	}
}

void AShooterCharacter::Turn(float Val)
{
	if (!bIsFreezing)
	{
		AddControllerYawInput(Val);
	}
}

void AShooterCharacter::LookUp(float Val)
{
	if (!bIsFreezing)
	{
		AddControllerPitchInput(Val);
	}
}

void AShooterCharacter::OnStartFire()
{
	AShooterPlayerController* MyPC = Cast<AShooterPlayerController>(Controller);
	if (MyPC && MyPC->IsGameInputAllowed())
	{
		if(!bIsFreezing)
		{
			if (IsRunning())
			{
				SetRunning(false, false);
			}
			StartWeaponFire();
		}
		else
		{
			GetWorld()->GetFirstPlayerController()->PlayerCameraManager->PlayCameraShake(FreezeCameraShake);
		}
		
	}
}

void AShooterCharacter::OnStopFire()
{
	StopWeaponFire();
}

void AShooterCharacter::OnStartTargeting()
{
	AShooterPlayerController* MyPC = Cast<AShooterPlayerController>(Controller);
	if (MyPC && MyPC->IsGameInputAllowed())
	{
		if(!bIsFreezing)
		{
			if (IsRunning())
			{
				SetRunning(false, false);
			}
			SetTargeting(true);
		}
		else
		{
			GetWorld()->GetFirstPlayerController()->PlayerCameraManager->PlayCameraShake(FreezeCameraShake);
		}
		
	}
}

void AShooterCharacter::OnStopTargeting()
{
	SetTargeting(false);
}

void AShooterCharacter::OnNextWeapon()
{
	AShooterPlayerController* MyPC = Cast<AShooterPlayerController>(Controller);
	if (MyPC && MyPC->IsGameInputAllowed())
	{
		if(!bIsFreezing)
		{
			if (Inventory.Num() >= 2 && (CurrentWeapon == NULL || CurrentWeapon->GetCurrentState() != EWeaponState::Equipping))
			{
				const int32 CurrentWeaponIdx = Inventory.IndexOfByKey(CurrentWeapon);
				AShooterWeapon* NextWeapon = Inventory[(CurrentWeaponIdx + 1) % Inventory.Num()];
				EquipWeapon(NextWeapon);
			}
		}
		else
		{
			GetWorld()->GetFirstPlayerController()->PlayerCameraManager->PlayCameraShake(FreezeCameraShake);
		}
		
	}
}

void AShooterCharacter::OnPrevWeapon()
{
	AShooterPlayerController* MyPC = Cast<AShooterPlayerController>(Controller);
	if (MyPC && MyPC->IsGameInputAllowed())
	{
		if(!bIsFreezing)
		{
			if (Inventory.Num() >= 2 && (CurrentWeapon == NULL || CurrentWeapon->GetCurrentState() != EWeaponState::Equipping))
			{
				const int32 CurrentWeaponIdx = Inventory.IndexOfByKey(CurrentWeapon);
				AShooterWeapon* PrevWeapon = Inventory[(CurrentWeaponIdx - 1 + Inventory.Num()) % Inventory.Num()];
				EquipWeapon(PrevWeapon);
			}
		}
		else
		{
			GetWorld()->GetFirstPlayerController()->PlayerCameraManager->PlayCameraShake(FreezeCameraShake);
		}
	}
}

void AShooterCharacter::OnReload()
{
	AShooterPlayerController* MyPC = Cast<AShooterPlayerController>(Controller);
	if (MyPC && MyPC->IsGameInputAllowed())
	{
		if(!bIsFreezing)
		{
			if (CurrentWeapon)
			{
				CurrentWeapon->StartReload();
			}
		}
		else
		{
			GetWorld()->GetFirstPlayerController()->PlayerCameraManager->PlayCameraShake(FreezeCameraShake);
		}
		
	}
}

void AShooterCharacter::OnStartRunning()
{
	AShooterPlayerController* MyPC = Cast<AShooterPlayerController>(Controller);
	if (MyPC && MyPC->IsGameInputAllowed())
	{
		if(!bIsFreezing)
		{
			if (IsTargeting())
			{
				SetTargeting(false);
			}
			StopWeaponFire();
			SetRunning(true, false);
		}
		else
		{
			GetWorld()->GetFirstPlayerController()->PlayerCameraManager->PlayCameraShake(FreezeCameraShake);
		}
		
	}
}

void AShooterCharacter::OnStartRunningToggle()
{
	AShooterPlayerController* MyPC = Cast<AShooterPlayerController>(Controller);
	if (MyPC && MyPC->IsGameInputAllowed())
	{
		if(!bIsFreezing)
		{
			if (IsTargeting())
			{
				SetTargeting(false);
			}
			StopWeaponFire();
			SetRunning(true, true);
		}
		else
		{
			GetWorld()->GetFirstPlayerController()->PlayerCameraManager->PlayCameraShake(FreezeCameraShake);
		}
	}
}

void AShooterCharacter::OnStopRunning()
{
	SetRunning(false, false);
}

bool AShooterCharacter::IsRunning() const
{
	if (!GetCharacterMovement())
	{
		return false;
	}

	return (bWantsToRun || bWantsToRunToggled) && !GetVelocity().IsZero() && (GetVelocity().GetSafeNormal2D() | GetActorForwardVector()) > -0.1;
}

void AShooterCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	// if NotifyHit wasn't called player is no longer touching a wall
	if(GetLocalRole() >= ROLE_AutonomousProxy && bOnWall && !bNotifyHitCalled)
	{
		bOnWall = false;

		if(bInWallRun)
		{
			StopWallRun();
		}
	}

	// set the variable to check if it get modified by NotifyHit the next tick
	bNotifyHitCalled = false;

	// update shrinking timer
	if(bIsShrinked)
	{
		ShrinkRemainingTime = FMath::Max(ShrinkRemainingTime - DeltaSeconds, 0.f);
	}

	// interpolate scaling when started shrinking
	if(bInShrinkingProcess)
	{
		float InterpCompletionRate = 1 - FMath::Clamp((ShrinkRemainingTime / ShrinkingInterpolationTime), 0.f, 1.f);

		if(InterpCompletionRate > 0){
			FVector InterpScale;

			InterpScale.X = FMath::Lerp(InitialScale.X, TargetScale.X, InterpCompletionRate);
			InterpScale.Y = FMath::Lerp(InitialScale.Y, TargetScale.Y, InterpCompletionRate);
			InterpScale.Z = FMath::Lerp(InitialScale.Z, TargetScale.Z, InterpCompletionRate);

			SetActorScale3D(InterpScale);

			BaseEyeHeight = FMath::Lerp(InitialEyeHeight, TargetEyeHeight, InterpCompletionRate);
		}
	}

	if (bWantsToRunToggled && !IsRunning())
	{
		SetRunning(false, false);
	}
	AShooterPlayerController* MyPC = Cast<AShooterPlayerController>(Controller);
	if (MyPC && MyPC->HasHealthRegen())
	{
		if (this->Health < this->GetMaxHealth())
		{
			this->Health += 5 * DeltaSeconds;
			if (Health > this->GetMaxHealth())
			{
				Health = this->GetMaxHealth();
			}
		}
	}

	if (GEngine->UseSound())
	{
		if (LowHealthSound)
		{
			if ((this->Health > 0 && this->Health < this->GetMaxHealth() * LowHealthPercentage) && (!LowHealthWarningPlayer || !LowHealthWarningPlayer->IsPlaying()))
			{
				LowHealthWarningPlayer = UGameplayStatics::SpawnSoundAttached(LowHealthSound, GetRootComponent(),
					NAME_None, FVector(ForceInit), EAttachLocation::KeepRelativeOffset, true);
				if (LowHealthWarningPlayer)
				{
					LowHealthWarningPlayer->SetVolumeMultiplier(0.0f);
				}
			}
			else if ((this->Health > this->GetMaxHealth() * LowHealthPercentage || this->Health < 0) && LowHealthWarningPlayer && LowHealthWarningPlayer->IsPlaying())
			{
				LowHealthWarningPlayer->Stop();
			}
			if (LowHealthWarningPlayer && LowHealthWarningPlayer->IsPlaying())
			{
				const float MinVolume = 0.3f;
				const float VolumeMultiplier = (1.0f - (this->Health / (this->GetMaxHealth() * LowHealthPercentage)));
				LowHealthWarningPlayer->SetVolumeMultiplier(MinVolume + (1.0f - MinVolume) * VolumeMultiplier);
			}
		}

		UpdateRunSounds();
	}

	const APlayerController* PC = Cast<APlayerController>(GetController());
	const bool bLocallyControlled = (PC ? PC->IsLocalController() : false);
	const uint32 UniqueID = GetUniqueID();
	FAudioThread::RunCommandOnAudioThread([UniqueID, bLocallyControlled]()
	{
	    USoundNodeLocalPlayer::GetLocallyControlledActorCache().Add(UniqueID, bLocallyControlled);
	});
	
	TArray<FVector> PointsToTest;
	BuildPauseReplicationCheckPoints(PointsToTest);

	if (NetVisualizeRelevancyTestPoints == 1)
	{
		for (FVector PointToTest : PointsToTest)
		{
			DrawDebugSphere(GetWorld(), PointToTest, 10.0f, 8, FColor::Red);
		}
	}
}

void AShooterCharacter::BeginDestroy()
{
	Super::BeginDestroy();

	if (!GExitPurge)
	{
		const uint32 UniqueID = GetUniqueID();
		FAudioThread::RunCommandOnAudioThread([UniqueID]()
		{
			USoundNodeLocalPlayer::GetLocallyControlledActorCache().Remove(UniqueID);
		});
	}
}

void AShooterCharacter::OnStartJump()
{
	AShooterPlayerController* MyPC = Cast<AShooterPlayerController>(Controller);
	if (MyPC && MyPC->IsGameInputAllowed())
	{
		if(!bIsFreezing)
		{
			// start a wall jump if the player is currently touching a wall while in air
			if (bOnWall && bCanWallJump)
			{
				WallJump();

				if (GetLocalRole() != ROLE_Authority)
					ServerWallJump();

				// Set a wait time to avoid flooding
				bCanWallJump = false;

				FTimerHandle WallJumpHandle;
				GetWorld()->GetTimerManager().SetTimer(WallJumpHandle, this, &AShooterCharacter::WallJumpWait, 0.3f);
			} // activate jetpack if the player is currently in air
			else if (GetCharacterMovement()->IsFalling() && bCanJetpack)
			{
				ShooterCharacterMovement->bPressedJetpack = true;

				// Set a wait time to avoid flooding
				bCanJetpack = false;

				FTimerHandle JetpackHandle;
				GetWorld()->GetTimerManager().SetTimer(JetpackHandle, this, &AShooterCharacter::JetpackWait, 0.1f);
			}
			else
			{
				bPressedJump = true;
			}
		}
		else
		{
			GetWorld()->GetFirstPlayerController()->PlayerCameraManager->PlayCameraShake(FreezeCameraShake);
		}
	}
}

void AShooterCharacter::OnStopJump()
{
	if(ShooterCharacterMovement && ShooterCharacterMovement->InJetpack())
	{
		ShooterCharacterMovement->bPressedJetpack = false;
	}
	else
	{
		bPressedJump = false;
		StopJumping();
	}
}

//////////////////////////////////////////////////////////////////////////
// Replication

void AShooterCharacter::PreReplication(IRepChangedPropertyTracker & ChangedPropertyTracker)
{
	Super::PreReplication(ChangedPropertyTracker);

	// Only replicate this property for a short duration after it changes so join in progress players don't get spammed with fx when joining late
	DOREPLIFETIME_ACTIVE_OVERRIDE(AShooterCharacter, LastTakeHitInfo, GetWorld() && GetWorld()->GetTimeSeconds() < LastTakeHitTimeTimeout);
}

void AShooterCharacter::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	// only to local owner: weapon change requests are locally instigated, other clients don't need it
	DOREPLIFETIME_CONDITION(AShooterCharacter, Inventory, COND_OwnerOnly);

	// everyone except local owner: flag change is locally instigated
	DOREPLIFETIME_CONDITION(AShooterCharacter, bIsTargeting, COND_SkipOwner);
	DOREPLIFETIME_CONDITION(AShooterCharacter, bWantsToRun, COND_SkipOwner);

	DOREPLIFETIME_CONDITION(AShooterCharacter, LastTakeHitInfo, COND_Custom);

	// everyone
	DOREPLIFETIME(AShooterCharacter, CurrentWeapon);
	DOREPLIFETIME(AShooterCharacter, Health);

	DOREPLIFETIME(AShooterCharacter, bIsFreezing);
	DOREPLIFETIME(AShooterCharacter, bIsShrinked);
	DOREPLIFETIME_CONDITION(AShooterCharacter, PastLocations, COND_OwnerOnly);
}

bool AShooterCharacter::IsReplicationPausedForConnection(const FNetViewer& ConnectionOwnerNetViewer)
{
	if (NetEnablePauseRelevancy == 1)
	{
		APlayerController* PC = Cast<APlayerController>(ConnectionOwnerNetViewer.InViewer);
		check(PC);

		FVector ViewLocation;
		FRotator ViewRotation;
		PC->GetPlayerViewPoint(ViewLocation, ViewRotation);

		FCollisionQueryParams CollisionParams(SCENE_QUERY_STAT(LineOfSight), true, PC->GetPawn());
		CollisionParams.AddIgnoredActor(this);

		TArray<FVector> PointsToTest;
		BuildPauseReplicationCheckPoints(PointsToTest);

		for (FVector PointToTest : PointsToTest)
		{
			if (!GetWorld()->LineTraceTestByChannel(PointToTest, ViewLocation, ECC_Visibility, CollisionParams))
			{
				return false;
			}
		}

		return true;
	}

	return false;
}

void AShooterCharacter::OnReplicationPausedChanged(bool bIsReplicationPaused)
{
	GetMesh()->SetHiddenInGame(bIsReplicationPaused, true);
}

AShooterWeapon* AShooterCharacter::GetWeapon() const
{
	return CurrentWeapon;
}

int32 AShooterCharacter::GetInventoryCount() const
{
	return Inventory.Num();
}

AShooterWeapon* AShooterCharacter::GetInventoryWeapon(int32 index) const
{
	return Inventory[index];
}

USkeletalMeshComponent* AShooterCharacter::GetPawnMesh() const
{
	return IsFirstPerson() ? Mesh1P : GetMesh();
}

USkeletalMeshComponent* AShooterCharacter::GetSpecifcPawnMesh(bool WantFirstPerson) const
{
	return WantFirstPerson == true ? Mesh1P : GetMesh();
}

FName AShooterCharacter::GetWeaponAttachPoint() const
{
	return WeaponAttachPoint;
}

float AShooterCharacter::GetTargetingSpeedModifier() const
{
	return TargetingSpeedModifier;
}

bool AShooterCharacter::IsTargeting() const
{
	return bIsTargeting;
}

float AShooterCharacter::GetRunningSpeedModifier() const
{
	return RunningSpeedModifier;
}

bool AShooterCharacter::IsFiring() const
{
	return bWantsToFire;
};

bool AShooterCharacter::IsFirstPerson() const
{
	return IsAlive() && Controller && Controller->IsLocalPlayerController();
}

int32 AShooterCharacter::GetMaxHealth() const
{
	return GetClass()->GetDefaultObject<AShooterCharacter>()->Health;
}

bool AShooterCharacter::IsAlive() const
{
	return Health > 0;
}

float AShooterCharacter::GetLowHealthPercentage() const
{
	return LowHealthPercentage;
}

void AShooterCharacter::UpdateTeamColorsAllMIDs()
{
	for (int32 i = 0; i < MeshMIDs.Num(); ++i)
	{
		UpdateTeamColors(MeshMIDs[i]);
	}
}

void AShooterCharacter::BuildPauseReplicationCheckPoints(TArray<FVector>& RelevancyCheckPoints)
{
	FBoxSphereBounds Bounds = GetCapsuleComponent()->CalcBounds(GetCapsuleComponent()->GetComponentTransform());
	FBox BoundingBox = Bounds.GetBox();
	float XDiff = Bounds.BoxExtent.X * 2;
	float YDiff = Bounds.BoxExtent.Y * 2;

	RelevancyCheckPoints.Add(BoundingBox.Min);
	RelevancyCheckPoints.Add(FVector(BoundingBox.Min.X + XDiff, BoundingBox.Min.Y, BoundingBox.Min.Z));
	RelevancyCheckPoints.Add(FVector(BoundingBox.Min.X, BoundingBox.Min.Y + YDiff, BoundingBox.Min.Z));
	RelevancyCheckPoints.Add(FVector(BoundingBox.Min.X + XDiff, BoundingBox.Min.Y + YDiff, BoundingBox.Min.Z));
	RelevancyCheckPoints.Add(FVector(BoundingBox.Max.X - XDiff, BoundingBox.Max.Y, BoundingBox.Max.Z));
	RelevancyCheckPoints.Add(FVector(BoundingBox.Max.X, BoundingBox.Max.Y - YDiff, BoundingBox.Max.Z));
	RelevancyCheckPoints.Add(FVector(BoundingBox.Max.X - XDiff, BoundingBox.Max.Y - YDiff, BoundingBox.Max.Z));
	RelevancyCheckPoints.Add(BoundingBox.Max);
}

//////////////////////////////////////////////////////////////////////////
//   Teleport

void AShooterCharacter::OnTeleport() 
{
	if (bCanTeleport) 
	{
		// Set a cooldown for the ability
		bCanTeleport = false;

		FTimerHandle TeleportWait;
		GetWorld()->GetTimerManager().SetTimer(TeleportWait, this, &AShooterCharacter::TeleportCooldownFinished, TeleportCooldown);

		ShooterCharacterMovement->bPressedTeleport = true;
	}
}

void AShooterCharacter::TeleportCooldownFinished() 
{
	bCanTeleport = true;
}

//////////////////////////////////////////////////////////////////////////
//   Rewind

void AShooterCharacter::StoreCurrentLocation() 
{
	// pause the location storage while in rewind
	if (bInRewind)
		return;

	// make sure to memorize only the most recent N locations
	if (PastLocations.Num() >= MaxLocations)
	{
		PastLocations.Pop();
	}

	PastLocations.Insert(GetActorLocation(), 0);
}

void AShooterCharacter::OnRewind() 
{
	if (!bCanRewind || bInRewind)
		return;

	// set a cooldown for the ability
	bCanRewind = false;

	FTimerHandle RewindCooldownHandle;
	GetWorld()->GetTimerManager().SetTimer(RewindCooldownHandle, this, &AShooterCharacter::RewindCooldownFinished, RewindCooldown, false);

	// call the function on both the owning client and the server
	Rewind();

	if (GetLocalRole() != ROLE_Authority) 
	{
		ServerRewind();
	}
}

void AShooterCharacter::ServerRewind_Implementation() 
{
	Rewind();
}

void AShooterCharacter::Rewind() 
{
	if (PastLocations.Num() == 0)
		return;

	bInRewind = true;

	LocIndex = 0;

	// apply a RootMotionSource to move the player on the most recent recorded location
	FRootMotionSource_MoveToForce* RewindForce = new FRootMotionSource_MoveToForce();

	RewindForce->InstanceName = TEXT("Rewind");
	RewindForce->AccumulateMode = ERootMotionAccumulateMode::Override;
	RewindForce->Priority = 6;
	RewindForce->Duration = RewindTime / MaxLocations;
	RewindForce->FinishVelocityParams.Mode = ERootMotionFinishVelocityMode::SetVelocity;
	RewindForce->FinishVelocityParams.SetVelocity = FVector::ZeroVector;
	RewindForce->StartLocation = GetActorLocation();
	RewindForce->TargetLocation = PastLocations[LocIndex++];

	uint8 ForceHandle = GetCharacterMovement()->ApplyRootMotionSource(RewindForce);

	// start a looping timer to move to the other recorded locations
	GetWorld()->GetTimerManager().SetTimer(RewindHandle, this, &AShooterCharacter::RewindStep, RewindTime / MaxLocations, true);
}

void AShooterCharacter::RewindStep() 
{
	// finish the rewind when there are no more locations
	if (LocIndex >= PastLocations.Num()) 
	{
		GetWorld()->GetTimerManager().ClearTimer(RewindHandle);
		bInRewind = false;

		return;
	}

	// move to the previous location
	FRootMotionSource_MoveToForce* RewindForce = new FRootMotionSource_MoveToForce();

	RewindForce->InstanceName = TEXT("Rewind");
	RewindForce->AccumulateMode = ERootMotionAccumulateMode::Override;
	RewindForce->Priority = 6;
	RewindForce->Duration = RewindTime / MaxLocations;
	RewindForce->FinishVelocityParams.Mode = ERootMotionFinishVelocityMode::SetVelocity;
	RewindForce->FinishVelocityParams.SetVelocity = FVector::ZeroVector;
	RewindForce->StartLocation = GetActorLocation();
	RewindForce->TargetLocation = PastLocations[LocIndex++];

	uint8 ForceHandle = GetCharacterMovement()->ApplyRootMotionSource(RewindForce);
}

void AShooterCharacter::RewindCooldownFinished() 
{
	bCanRewind = true;
}


//////////////////////////////////////////////////////////////////////////
//   Jetpack

void AShooterCharacter::JetpackWait()
{
	bCanJetpack = true;
}


//////////////////////////////////////////////////////////////////////////
//   Freeze Gun

void AShooterCharacter::ServerFreeze() 
{
	if (bIsFreezing)
		return;

	// set the variable to stop taking inputs
	bIsFreezing = true;

	FreezeEffects();

	FTimerHandle FreezeTimer;
	GetWorld()->GetTimerManager().SetTimer(FreezeTimer, this, &AShooterCharacter::ServerUnfreeze, FreezeTime, false);
}

void AShooterCharacter::ServerUnfreeze() 
{
	if (!bIsFreezing)
		return;

	bIsFreezing = false;

	UnfreezeEffects();
}

void AShooterCharacter::OnRep_IsFreezing()
{
	if(bIsFreezing)
	{
		FreezeEffects();
	}
	else
	{
		UnfreezeEffects();
	}
}

void AShooterCharacter::FreezeEffects()
{
	FreezePSComponent->SetActive(true);
}

void AShooterCharacter::UnfreezeEffects()
{
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), FreezeBrokenParticleSystem, GetActorTransform(), true);
	FreezePSComponent->SetActive(false);
}

//////////////////////////////////////////////////////////////////////////
//   ShrinkGun

void AShooterCharacter::ServerShrink() 
{
	if (bIsShrinked)
		return;

	bIsShrinked = true;

	GetWorld()->GetTimerManager().SetTimer(ShrinkTimerHandle, this, &AShooterCharacter::ServerUnshrink, ShrinkTime, false);

	ShrinkRemainingTime = ShrinkTime;
	StartShrinkingProcess();
	
}

void AShooterCharacter::ServerUnshrink() 
{
	if (!bIsShrinked)
		return;

	bIsShrinked = false;

	ShrinkRemainingTime = 0.f;
	StartUnshrinkingProcess();
}

void AShooterCharacter::OnRep_Shrink()
{
	if (bIsShrinked) 
	{
		ShrinkRemainingTime = ShrinkTime;
		StartShrinkingProcess();
	}
	else 
	{
		ShrinkRemainingTime = 0.f;
		StartUnshrinkingProcess();
	}
}

void AShooterCharacter::StartShrinkingProcess()
{
	InitialScale = GetActorScale3D();
	TargetScale = InitialScale * ShrinkScale;
	InitialEyeHeight = BaseEyeHeight;
	TargetEyeHeight = ShrinkEyeHeight;

	bInShrinkingProcess = true;
	GetWorld()->GetTimerManager().SetTimer(ShrinkingProcessHandle, this, &AShooterCharacter::Shrink, ShrinkingInterpolationTime, false);
}

void AShooterCharacter::StartUnshrinkingProcess()
{
	InitialScale = GetActorScale3D();
	TargetScale = InitialScale / ShrinkScale;
	InitialEyeHeight = BaseEyeHeight;
	TargetEyeHeight = DefaultEyeHeight;

	bInShrinkingProcess = true;

	// reenable all the normal collisions
	GetMesh()->SetCollisionResponseToChannel(ECC_Pawn, ECR_Block);
	GetMesh()->SetCollisionResponseToChannel(COLLISION_WEAPON, ECR_Block);
	GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_Pawn, ECR_Block);
	CurrentWeapon->SetActorEnableCollision(true);


	GetWorld()->GetTimerManager().SetTimer(ShrinkingProcessHandle, this, &AShooterCharacter::Unshrink, ShrinkingInterpolationTime, false);
}

void AShooterCharacter::Shrink()
{
	bInShrinkingProcess = false;

	SetActorScale3D(TargetScale);
	BaseEyeHeight = TargetEyeHeight;

	// modify physic behaviour while shrinked, to avoid moving at the same speed of a bigger character
	if (ShooterCharacterMovement)
	{
		ShooterCharacterMovement->GravityScale /= 2.f;
		ShooterCharacterMovement->MaxWalkSpeed /= 1.8f;
		ShooterCharacterMovement->JumpZVelocity /= 2.f;
		ShooterCharacterMovement->JetpackImpulseForce /= 2.f;
	}

	WallRunDistance /= 2;
	WallJumpImpulseForce /= 2;

	// Set the collisions to let the player overlap with other players
	GetMesh()->SetCollisionResponseToChannel(ECC_Pawn, ECR_Ignore);
	GetMesh()->SetCollisionResponseToChannel(COLLISION_WEAPON, ECR_Ignore);
	GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	CurrentWeapon->SetActorEnableCollision(false);
}

void AShooterCharacter::Unshrink() 
{
	bInShrinkingProcess = false;
	
	SetActorScale3D(TargetScale);
	BaseEyeHeight = TargetEyeHeight;

	// set physic behaviour back to normal
	if(ShooterCharacterMovement)
	{
		ShooterCharacterMovement->GravityScale *= 2.f;
		ShooterCharacterMovement->MaxWalkSpeed *= 1.8f;
		ShooterCharacterMovement->JumpZVelocity *= 2.f;
		ShooterCharacterMovement->JetpackImpulseForce *= 2.f;
	}
	
	WallRunDistance *= 2;
	WallJumpImpulseForce *= 2;
}

void AShooterCharacter::NotifyActorBeginOverlap(class AActor* Other)
{
	Super::NotifyActorBeginOverlap(Other);

	// kill the player when shrinked and stomped upon
	AShooterCharacter *OtherPlayer = Cast<AShooterCharacter>(Other);
	if (OtherPlayer && GetLocalRole() == ROLE_Authority && bIsShrinked && !OtherPlayer->bIsShrinked)
	{
		FDamageEvent StompDamage;
		Die(Health, StompDamage, OtherPlayer->Controller, Other);
	}
}

//////////////////////////////////////////////////////////////////////////
//   Weapon Pickup

void AShooterCharacter::SpawnWeaponPickup() 
{
	if (GetLocalRole() == ROLE_Authority && WeaponPickup && CurrentWeapon) 
	{
		FActorSpawnParameters SpawnParams;
		SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		AShooterPickup_Weapon* Pickup = GetWorld()->SpawnActor<AShooterPickup_Weapon>(WeaponPickup, GetActorLocation(), GetActorRotation());

		if(Pickup)
		{
			Pickup->SetWeapon(CurrentWeapon);
		}
			
	}
}

//////////////////////////////////////////////////////////////////////////
// Wall Run

void AShooterCharacter::NotifyHit(class UPrimitiveComponent* MyComp, AActor* Other, class UPrimitiveComponent* OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult& Hit) 
{
	Super::NotifyHit(MyComp, Other, OtherComp, bSelfMoved, HitLocation, HitNormal, NormalImpulse, Hit);

	if(GetLocalRole() >= ROLE_AutonomousProxy) // Execute only on owning client and server
	{
		// check if the player is moving against a BlockingVolume on a face almost perpendicular to the floor (a wall) and is currently in air
		if (Other && bSelfMoved && Other->IsA(ABlockingVolume::StaticClass()) && abs(HitNormal.Z) < 0.05f && GetCharacterMovement()->IsFalling())
		{
			bNotifyHitCalled = true;
			WallNormal = HitNormal;

			// set this wall as a new one if the player just touched this wall
			if(!bOnWall)
			{
				bOnWall = true;
				bNewWall = true;
			}

			// start running on the wall if the player didn't yet runned on the current wall
			if (!bInWallRun && bCanWallRun && bNewWall)
			{
				WallRun();
			}

		}
	}
}

void AShooterCharacter::WallRun() 
{
	// don't start the run if the player is looking in the opposite direction of the wall
	if (FVector::DotProduct(WallNormal, GetActorForwardVector()) > 0.2f)
		return;

	// don't start the run if the player is looking at the wall almost perpendicularly
	float ViewAngle = FVector::DotProduct(WallNormal, GetActorRightVector());

	if (abs(ViewAngle) < 0.05f)
		return;

	bNewWall = false;
	bInWallRun = true;

	FTimerHandle WallRunHandle;
	GetWorld()->GetTimerManager().SetTimer(WallRunHandle, this, &AShooterCharacter::WallRunFinished, WallRunDuration);

	// apply a RootMotionSource to move the player on the wall
	FRootMotionSource_MoveToForce* WallRunForce = new FRootMotionSource_MoveToForce();

	WallRunForce->InstanceName = TEXT("WallRun");
	WallRunForce->AccumulateMode = ERootMotionAccumulateMode::Override;
	WallRunForce->Priority = 4;
	WallRunForce->Duration = WallRunDuration;
	WallRunForce->StartLocation = GetActorLocation();

	// select the direction for the wall run
	FVector WalkDirection = ViewAngle > 0 ? WallNormal.RotateAngleAxis(-95, FVector(0.f, 0.f, 1.f)) : WallNormal.RotateAngleAxis(95, FVector(0.f, 0.f, 1.f));

	// move the player N meters on the wall in the selected direction and a little upper than before
	WallRunForce->TargetLocation = GetActorLocation() + FVector(0.f, 0.f, 50.f) + WalkDirection * WallRunDistance;

	WallRunForceID = GetCharacterMovement()->ApplyRootMotionSource(WallRunForce);

	// set a cooldown for the ability
	bCanWallRun = false;

	FTimerHandle WallRunCooldownHandle;
	GetWorld()->GetTimerManager().SetTimer(WallRunCooldownHandle, this, &AShooterCharacter::WallRunCooldownFinished, WallRunCooldown);
}

void AShooterCharacter::WallRunFinished()
{
	bInWallRun = false;
}

void AShooterCharacter::StopWallRun() 
{
	GetCharacterMovement()->RemoveRootMotionSourceByID(WallRunForceID);
}

void AShooterCharacter::WallRunCooldownFinished() 
{
	bCanWallRun = true;
}

//////////////////////////////////////////////////////////////////////////
//   Wall Jump

void AShooterCharacter::ServerWallJump_Implementation() 
{
	WallJump();
}

void AShooterCharacter::WallJump()
{
	// apply a RootMotionSource to jump 
	FRootMotionSource_ConstantForce *WallJumpForce = new FRootMotionSource_ConstantForce();

	WallJumpForce->InstanceName = TEXT("WallJump");
	WallJumpForce->AccumulateMode = ERootMotionAccumulateMode::Additive;
	WallJumpForce->Priority = 5;
	WallJumpForce->Duration = 0.1f;

	// apply a force directed 40� on the normal of the wall
	WallJumpForce->Force = WallNormal.RotateAngleAxis(-40.f, WallNormal.RotateAngleAxis(90.f, FVector(0.f, 0.f, 1.f))) * WallJumpImpulseForce;

	// remove the run wall force if the player was running on a wall before jumping
	GetCharacterMovement()->RemoveRootMotionSourceByID(WallRunForceID);
	uint8 ForceHandle = GetCharacterMovement()->ApplyRootMotionSource(WallJumpForce);
}

void AShooterCharacter::WallJumpWait()
{
	bCanWallJump = true;
}
