// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "Pickups/ShooterPickup_Weapon.h"
#include "Weapons/ShooterWeapon.h"
#include "OnlineSubsystemUtils.h"

AShooterPickup_Weapon::AShooterPickup_Weapon(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	WeaponMesh = ObjectInitializer.CreateDefaultSubobject<USkeletalMeshComponent>(this, TEXT("WeaponMesh"));
	WeaponMesh->SetupAttachment(RootComponent);
	WeaponMesh->SetRelativeLocation(FVector(0.f, -30.f, 0.f));

	WeaponMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	WeaponMesh->SetCollisionResponseToAllChannels(ECR_Ignore);
	
	RespawnTime = 0.f;

	bWeaponSet = false;
}

void AShooterPickup_Weapon::GetLifetimeReplicatedProps(TArray< FLifetimeProperty >& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AShooterPickup_Weapon, WeaponMesh);
	DOREPLIFETIME(AShooterPickup_Weapon, bWeaponSet);
}

void AShooterPickup_Weapon::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	// Destroy the pickup after n seconds
	if(GetLocalRole() == ROLE_Authority)
	{
		FTimerHandle DestroyTimer;
		GetWorld()->GetTimerManager().SetTimer(DestroyTimer, this, &AShooterPickup_Weapon::DestroyPickup, Lifetime);
	}
}

void AShooterPickup_Weapon::SetWeapon(const class AShooterWeapon *Weapon)
{
	if(Weapon){
		WeaponType = Weapon->GetClass();
		WeaponMesh->SetSkeletalMesh(Weapon->GetWeaponMesh()->SkeletalMesh);
		AmmoAmount = Weapon->GetCurrentAmmo();

		bWeaponSet = true;
	}
	else{
		Destroy();
	}
}

void AShooterPickup_Weapon::PickupOnTouch(class AShooterCharacter *Pawn)
{
	if (bWeaponSet && bIsActive && Pawn && Pawn->IsAlive() && !IsPendingKill())
	{
		if (CanBePickedUp(Pawn))
		{
			if(GetLocalRole() == ROLE_Authority)
			{
				GivePickupTo(Pawn);
			}
				
			PickedUpBy = Pawn;

			if (!IsPendingKill())
			{
				bIsActive = false;
				OnPickedUp();

				if(GetLocalRole() == ROLE_Authority)
				{
					Destroy();
				}	
			}
			
		}
	}
}

void AShooterPickup_Weapon::GivePickupTo(class AShooterCharacter* Pawn)
{
	AShooterWeapon* Weapon = Pawn->FindWeapon(WeaponType);

	// if the player already has the current weapon, increase ammo amount, else add the weapon to inventory
	if (Weapon)
	{
		Weapon->GiveAmmo(AmmoAmount);
	}
	else 
	{
		AddWeapon(Pawn);
	}
}

void AShooterPickup_Weapon::AddWeapon(class AShooterCharacter* Pawn)
{
	FActorSpawnParameters SpawnInfo;
	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	AShooterWeapon* NewWeapon = GetWorld()->SpawnActor<AShooterWeapon>(WeaponType, SpawnInfo);
	Pawn->AddWeapon(NewWeapon);
}

void AShooterPickup_Weapon::DestroyPickup()
{
	Destroy();
}









